//
//  MMTCSGradientView.m
//  Mobimed
//
//  Created by Androide Osorio, Sebastian Zapata on 9/05/12.
//  Copyright (c) 2012 androide Osorio, SebaZ. All rights reserved.
//

#import "MMTCSGradientView.h"

@implementation MMTCSGradientView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    //get the drawing context (like a canvas)
	CGContextRef context = UIGraphicsGetCurrentContext();
    
    //save the current state
	CGContextSaveGState(context);
    //get the color space as RGB
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    //create the Gradient information
	CGGradientRef gradient = CGGradientCreateWithColorComponents
	(colorSpace, /*[UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1] , [UIColor colorWithRed:10.0/255 green:142.0/255 blue:141.0/255 alpha:1],2);*/
	 (const CGFloat[]){240.0/255.0,240.0/255.0,240.0/255.0, 1.0},
	 (const CGFloat[]){253.0/255.0,253.0/255.0,253.0/255.0, 1.0},
	 2);
    
    //draw the linear gradient
	CGContextDrawLinearGradient(context,
								gradient,
								CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMinY(self.bounds)),
								CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMaxY(self.bounds)),
								0);
    //reset the color space
	CGColorSpaceRelease(colorSpace);
	CGContextRestoreGState(context);
}


@end
