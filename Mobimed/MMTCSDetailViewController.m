//
//  MMTCSDetailViewController.m
//  Mobimed
//
//  Created by Androide Osorio on 9/05/12.
//  Copyright (c) 2012 Androide Osorio, sebaZ. All rights reserved.
//

#import "MMTCSDetailViewController.h"
#import "MMTCSVideoCallViewController.h"
#import "MMTCSPatientsTileView.h"
#import "MMTCSMasterViewController.h"
#import "MMTCSMedicalHistoryDetailViewController.h"
#import "MMTCSPatientInfoViewController.h"
#import "MMTCSMedicalHistoryMasterViewController.h"

#pragma mark private category declaration
@interface MMTCSDetailViewController ()

@property (strong, nonatomic) UIPopoverController *masterPopoverController;

- (void)configureView;

- (void)scrollViewStyle;

- (void)layoutScrollImages:(UIScrollView*)scrollView withDimensionsAdjustment:(BOOL)adjust andTilePadding:(BOOL)allow;

- (void)loadScrollViewImages:(UIScrollView *)view withNumOfImages:(NSUInteger)numImages andPresentationStyle:(PatientsTileViewPresentationStyle)style;

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation;

- (void)handleDataModelChange:(NSNotification *)note;

-(void)updateTileViewsWithManagedObject:(NSArray *)managedObjects inScrollView:(UIScrollView *)scrollView  andPresentationStyle:(PatientsTileViewPresentationStyle)style;

@end

//---------------------------------------------------------------------------------------

@implementation MMTCSDetailViewController{
    UIImageView *instructions;
}

#pragma mark - properties and utility variables
const CGFloat ObjWidth = 256;
const CGFloat ObjHeight = 386.0;

@synthesize detailItem = _detailItem;
@synthesize detailDescriptionLabel = _detailDescriptionLabel;
@synthesize todaysAppointmentsScrollView = _todaysAppointmentsScrollView;
@synthesize futureAppointmentsScrollView = _futureAppointmentsScrollView;
@synthesize todaysAppointmentsLabel = _todaysAppointmentsLabel;
@synthesize futureAppointmentsLabel = _futureAppointmentsLabel;
@synthesize masterPopoverController = _masterPopoverController;
@synthesize managedObjectContext;
@synthesize patients;
@synthesize selectedPatient;
@synthesize showInstructions;

//--------------------------------------------------------------
#pragma mark - Memory Management
- (void)dealloc
{
    [_detailItem release];
    [_detailDescriptionLabel release];
    [_masterPopoverController release];
    
    [_todaysAppointmentsScrollView release];
    [_futureAppointmentsScrollView release];
    [_todaysAppointmentsLabel release];
    [_futureAppointmentsLabel release];
    [patients release];
    [managedObjectContext release];
    [super dealloc];
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        [_detailItem release]; 
        _detailItem = [newDetailItem retain]; 
        
        // Update the view.
        [self configureView];
    }
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.
    
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    self.patients = [NSMutableArray array];
    
    [self performSegueWithIdentifier:@"Login" sender:nil];
    
    //set the Scroll view styles of todays patients
	[self.todaysAppointmentsLabel.layer setMasksToBounds:NO];
	self.todaysAppointmentsLabel.layer.shadowColor= [UIColor blackColor].CGColor;
	self.todaysAppointmentsLabel.layer.shadowOpacity=0.7;
	self.todaysAppointmentsLabel.layer.shadowOffset = CGSizeMake(0.0,1.0);	
	self.todaysAppointmentsLabel.layer.shadowRadius =3.0;
	
    //set the scroll view styles of incoming patients
	[self.futureAppointmentsLabel.layer setMasksToBounds:NO];
	self.futureAppointmentsLabel.layer.shadowColor= [UIColor blackColor].CGColor;
	self.futureAppointmentsLabel.layer.shadowOpacity=0.7;
	self.futureAppointmentsLabel.layer.shadowOffset=CGSizeMake(0.0, 1.0);
	self.futureAppointmentsLabel.layer.shadowRadius=3.0;
    
    //layout the scrollview subviews
    [self scrollViewStyle];
    [self loadScrollViewImages:self.todaysAppointmentsScrollView withNumOfImages:6 andPresentationStyle:PatientsTileViewPresentationStylePhoto];
    [self loadScrollViewImages:self.futureAppointmentsScrollView withNumOfImages:6 andPresentationStyle:PatientsTileViewPresentationStyleSimple];
    [self layoutScrollImages:self.todaysAppointmentsScrollView withDimensionsAdjustment:NO andTilePadding:NO];
    [self layoutScrollImages:self.futureAppointmentsScrollView withDimensionsAdjustment:NO andTilePadding:YES];
    
    //arrange the detail view components
    [self.view setBackgroundColor:[UIColor colorWithRed:227.0/255 green:229.0/255 blue:226.0/255 alpha:1]];
    [self.view sendSubviewToBack:self.todaysAppointmentsScrollView];
    [self.view sendSubviewToBack:self.futureAppointmentsScrollView];
    
    //add an observer for incoming changes in the data model
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(handleDataModelChange:) 
                                                 name:NSManagedObjectContextObjectsDidChangeNotification 
                                               object:self.managedObjectContext];
    
    instructions = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DashboardLandscapeInstructions.png"]];
    [self.view addSubview:instructions];
    instructions.hidden = YES;
    
    self.showInstructions = NO;
}

- (void)viewDidUnload
{
    
    [self setTodaysAppointmentsScrollView:nil];
    [self setFutureAppointmentsScrollView:nil];
    [self setTodaysAppointmentsLabel:nil];
    [self setFutureAppointmentsLabel:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255 blue:85.0/255 alpha:1];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    // Return YES for supported orientations
    [self adjustViewsForOrientation:interfaceOrientation];
    return YES;
}

//---------------------------------------------------------------------------------------------------------
#pragma mark - SplitViewControllerDelegate Methods

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Doctor", @"Doctor");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

//---------------------------------------------------
/**
 *
 */
#pragma mark - Custom View Controller Delegate Methods

-(void)loginViewControllerDidMadeLoginRequest:(MMTCSLoginViewController *)controller
{
    //call the login from model
    //--

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)MMTCSAddAppointmentViewControllerDidCancel:(MMTCSAddAppointmentViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)MMTCSAddAppointmentViewControllerDidCreate:(MMTCSAddAppointmentViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addPatientDidSubmit:(MMTCSAddPatientViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addPatientDidCancel:(MMTCSAddPatientViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------
/**
 * Storyboard handling methods
 */
#pragma mark - Storyboard Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Login"])
	{
		MMTCSLoginViewController *loginView = segue.destinationViewController;
		loginView.delegate = self;
	}
    if([segue.identifier isEqualToString:@"AddAppointment"])
    {
        UINavigationController* navigationController = segue.destinationViewController;
        MMTCSAddAppointmentViewController* addView = [[navigationController viewControllers] objectAtIndex:0];
        addView.delegate = self;
    }
    if([segue.identifier isEqualToString:@"AddPatient"])
    {
        UINavigationController* navigationController = segue.destinationViewController;
        MMTCSAddPatientViewController* addView = [[navigationController viewControllers] objectAtIndex:0];
        addView.delegate = self;
    }
    if([segue.identifier isEqualToString:@"gotoVideo"])
    {
        MMTCSVideoCallViewController* destination = segue.destinationViewController;
        [destination setCurrentPatient:[self.patients objectAtIndex:self.selectedPatient]];
        self.splitViewController.delegate = destination;
    }
    if([segue.identifier isEqualToString:@"gotoHistory"])
    {
        MMTCSMedicalHistoryDetailViewController* destination = segue.destinationViewController;
        self.splitViewController.delegate = destination;
        destination.currentPatient = [self.patients objectAtIndex:self.selectedPatient];
    }
}

//---------------------------------------------------------------------------
////
///


#pragma mark - Private Methods
//adjust the interface orientation correctly
- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation
{
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        [self.todaysAppointmentsScrollView setFrame:CGRectMake(0, 45, 780, 386)]; //386
        self.futureAppointmentsScrollView.backgroundColor = [UIColor clearColor];
        [self.futureAppointmentsScrollView setFrame:CGRectMake(0, 490, 690, 200)];
        [_futureAppointmentsLabel setFrame:CGRectMake(0, 430, 950, 50)];
        //[_futureAppointmentsLabel setBackgroundColor:[UIColor clearColor]];
        
        [self layoutScrollImages:self.futureAppointmentsScrollView withDimensionsAdjustment:NO andTilePadding:YES];
        [self layoutScrollImages:self.todaysAppointmentsScrollView withDimensionsAdjustment:YES andTilePadding:NO];
        
        [instructions setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [instructions setImage:[UIImage imageNamed:@"DashboardLandscapeInstructions.png"]];
        
    }
    else if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){   
        [instructions setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [instructions setImage:[UIImage imageNamed:@"Dashboard_portraitInstructions.png"]];
        [self.todaysAppointmentsScrollView setFrame:CGRectMake(0, 45, 768, 386)];
        [self.futureAppointmentsScrollView setFrame:CGRectMake(0, 490, 780, 430)]; //386
        [_futureAppointmentsLabel setFrame:CGRectMake(0, 430, 950, 50)];
        self.futureAppointmentsScrollView.backgroundColor = [UIColor clearColor];
        
        MMTCSPatientsTileView *view= nil;
        NSArray *subviews = [self.futureAppointmentsScrollView subviews];
        int cont =0;
        CGFloat curXloc = 50;
        CGFloat curYloc = 0;
        for (view in subviews) {
            view.tag = cont+1;
            if ([view isKindOfClass:[MMTCSPatientsTileView class]] && view.tag>0){
                CGRect frame = view.frame;
                cont++;
                if(cont % 4 == 0){
                    curXloc =50;
                    curYloc += frame.size.height;
                }
                frame.origin = CGPointMake(curXloc, curYloc);
                view.frame = frame;
                curXloc += (view.frame.size.width);
                curXloc += 20;
            }
        }
        [self.futureAppointmentsScrollView setContentSize:CGSizeMake( 100, ([subviews count] *55))];
        
    }
}//cierra metodo


-(void)scrollViewStyle
{
    // estilos scrollPacientesProximos
    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255 blue:85.0/255 alpha:1];

    [self.futureAppointmentsScrollView setBackgroundColor:[UIColor clearColor]];
    [self.futureAppointmentsScrollView setCanCancelContentTouches:NO];
    self.futureAppointmentsScrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    self.futureAppointmentsScrollView.clipsToBounds = YES;
    self.futureAppointmentsScrollView.scrollEnabled = YES;
    self.futureAppointmentsScrollView.pagingEnabled = YES;
    
    //estilos scroll pacientes hoy
    [self.todaysAppointmentsScrollView setBackgroundColor:[UIColor clearColor]];
    [self.todaysAppointmentsScrollView setCanCancelContentTouches:NO];
    self.todaysAppointmentsScrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    self.todaysAppointmentsScrollView.clipsToBounds = YES;
    self.todaysAppointmentsScrollView.scrollEnabled = YES;
    self.todaysAppointmentsScrollView.pagingEnabled = YES;
}

//---------------------------------------------------------------------------
#pragma mark - Layout and resizing methods

- (void)layoutScrollImages:(UIScrollView*)scrollView withDimensionsAdjustment:(BOOL)adjust andTilePadding:(BOOL)allow
{
    MMTCSPatientsTileView *view= nil;
    NSArray *todaysPatientsSubviews = [scrollView subviews];
    CGFloat curXloc = 0;
    
    //obtiene views para Pacientes hoy
    for (view in todaysPatientsSubviews) {
        if ([view isKindOfClass:[MMTCSPatientsTileView class]]){
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXloc, 0);
            view.frame = frame;
            curXloc += (view.frame.size.width);
            
            if(allow){
                curXloc += 20;
            }
        }
    }
    
    [scrollView setContentSize:CGSizeMake(([todaysPatientsSubviews count] * ObjWidth), 200)];  //width era 190
    //scrollLanscapeWidth = ([todaysPatientsSubviews count] * 256.0);
}

- (void)loadScrollViewImages:(UIScrollView *)view withNumOfImages:(NSUInteger)numImages andPresentationStyle:(PatientsTileViewPresentationStyle)style;
{
    NSUInteger i;
    for (i = 1; i <= numImages; i++) {
        NSString *imageName = [NSString stringWithFormat:@"patient_photo_%d.png",i];
        UIImage *image = [UIImage imageNamed:imageName];
        
        // añade info pacientes hoy
        
        MMTCSPatientsTileView* patientContainer = [[MMTCSPatientsTileView alloc]initWithPresentationStyle:(style == PatientsTileViewPresentationStylePhoto) ?PatientsTileViewPresentationStylePhoto : PatientsTileViewPresentationStyleSimple];
        [patientContainer setPatientImage:image 
                                 withInfo:@"Pepita Perez" 
                                 dateHour:@"3:20pm" 
                                    place:@"Clinica valle del Lili" 
                                 duration:@"1hora" 
                                  andDate:@"3 de Mayo de 2012"];
        
        if(patientContainer.presentationStyle != PatientsTileViewPresentationStyleSimple){
            [patientContainer addTarget:self action:@selector(tapAreaSensible:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            [patientContainer addTarget:self action:@selector(tapFutureAppointment:) forControlEvents:UIControlEventTouchUpInside];
        }
        [view addSubview:patientContainer];
        [patientContainer release];
    }
}


/**
 * This method updates the UIScrollView subviews to the data the core data Model just fetched
 */
-(void)updateTileViewsWithManagedObject:(NSArray *)managedObjects inScrollView:(UIScrollView *)scrollView andPresentationStyle:(PatientsTileViewPresentationStyle)style
{
    //remove the previous subviews first
    for (UIView *subview in scrollView.subviews) {
        [subview removeFromSuperview];
    }
    
    //loop through every new subview
    NSUInteger i = 0;
    NSUInteger newTag = 1;
    for (i = 0; i < [managedObjects count]; i++) {
        //if the new inserted objects are Appointments
        if([[managedObjects objectAtIndex:i] isKindOfClass:[Appointment class]])
        {
            //Make the appropiate cast to the Appointment Class and create the initial patientTileView
            Appointment* managedObject      = (Appointment *)[managedObjects objectAtIndex:i];
            MMTCSPatientsTileView *tileView = [[MMTCSPatientsTileView alloc] initWithPresentationStyle:style];
            
            tileView.tag = newTag;
            NSLog(@"tile view tag : %d - %d", [managedObject.id intValue],tileView.tag);
            newTag++;
            //load the ipatient image from the download destination path (in Documents)
            UIImage *image      = [UIImage imageWithContentsOfFile:[[managedObject valueForKey:@"Patient"] valueForKey:@"photo"]];
            
            //the date and hour formatters
            NSDateFormatter *hourFormat = [[NSDateFormatter alloc] init];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            NSString *hora = [NSString stringWithFormat:@"%@ Hora", managedObject.duration];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [hourFormat setDateFormat:@"HH:mm"];
            
            //if the title view corresponds to a PatientsTileView
            if([tileView isKindOfClass:[MMTCSPatientsTileView class]]){
                //load the data to the new PatientsTileView
                [tileView setPatientImage:image
                                 withInfo:managedObject.patient.name
                                 dateHour:[hourFormat stringFromDate:managedObject.date]
                                    place:managedObject.place
                                 duration:hora
                                  andDate:[dateFormat stringFromDate:managedObject.date]];
                
                //Add the target Actions for the Action Sheet events
                if(tileView.presentationStyle != PatientsTileViewPresentationStyleSimple){
                    [tileView addTarget:self action:@selector(tapAreaSensible:) forControlEvents:UIControlEventTouchUpInside];
                }
                else{
                    [tileView addTarget:self action:@selector(tapFutureAppointment:) forControlEvents:UIControlEventTouchUpInside];
                }
                [scrollView addSubview:tileView];
            }
        }
    }
    /**
     * make the scrollviews layout again as we cleared them.
     */
    if(style == PatientsTileViewPresentationStylePhoto){
        [self layoutScrollImages:scrollView withDimensionsAdjustment:NO andTilePadding:NO];
    }
    else{
        [self layoutScrollImages:scrollView withDimensionsAdjustment:NO andTilePadding:YES];
    } 
}


//--------------------------------------------------------------------------------------------------------------------
#pragma mark - UIActionSheetDelegate Methods

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //get the actual patient
    //get the master view controller
    id masterViewController = [self.splitViewController.viewControllers objectAtIndex:0];
    
    if(buttonIndex == 0){
        //go to the videoconference screens
        [self performSegueWithIdentifier:@"gotoVideo" sender:nil];
        //load the appropiate master view controller
        if([masterViewController isKindOfClass:[UINavigationController class]])
        {
            UINavigationController* nav = (UINavigationController*)masterViewController;
            /*
            if([[nav.viewControllers objectAtIndex:1] isKindOfClass:[MMTCSPatientInfoViewController class]]){
                MMTCSPatientInfoViewController *patientInfo = (MMTCSPatientInfoViewController *)[nav.viewControllers objectAtIndex:0];
                patientInfo.currentPatient = patient;
                
            }*/
            [[nav.viewControllers objectAtIndex:0] performSegueWithIdentifier:@"gotoPatientInfo" sender:self];
        }
    }
    else if(buttonIndex == 1){
        //goto the show consultations history
        [self performSegueWithIdentifier:@"gotoHistory" sender:nil];
        //load the appropiate master view controller
        if([masterViewController isKindOfClass:[UINavigationController class]])
        {
            UINavigationController* nav = (UINavigationController*)masterViewController;
            
            /*if([[nav.viewControllers objectAtIndex:1] isKindOfClass:[MMTCSMedicalHistoryMasterViewController class]]){
                MMTCSMedicalHistoryMasterViewController *patientInfo = (MMTCSMedicalHistoryMasterViewController *)[nav.viewControllers objectAtIndex:0];
                patientInfo.currentPatient = patient;
            }*/
            [[nav.viewControllers objectAtIndex:0] performSegueWithIdentifier:@"gotoPatientHistory" sender:self];
        }
    }
}

//-------------------------------------------------------------------------------------
#pragma mark - Action methods

-(IBAction)tapAreaSensible:(id)sender
{
    MMTCSPatientsTileView *patient = (MMTCSPatientsTileView*)sender;
    selectedPatient = [patient tag]-1;
    NSLog(@"el tag del presionado es: %d", selectedPatient);
    Patient *thePatient = [patients objectAtIndex:selectedPatient];
    NSLog(@"el paciente es: %@", thePatient);
    
    //create the action sheet with the options
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Ver" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    
    //add the buttons for the actions
    [action addButtonWithTitle:@"Video-consulta"];
    [action addButtonWithTitle:@"Historial"];
    
    //reposition the action sheet to the specified tile view and show it
    CGRect frame;
    frame = patient.frame;
    if(frame.origin.x >= self.todaysAppointmentsScrollView.frame.size.width-20){
        frame.origin = CGPointMake(frame.origin.x - self.todaysAppointmentsScrollView.frame.size.width, patient.frame.origin.y);
    }  
    [action showFromRect:frame inView:self.view animated:YES];
    [action release];
    
}


-(IBAction)tapFutureAppointment:(id)sender
{
    //get the patient the user actually tapped, according to the view's tag
    MMTCSPatientsTileView *patient = (MMTCSPatientsTileView*)sender;
    selectedPatient = [patient tag]-1;
    //NSLog(@"el tag del presionado es: %d", selectedPatient);
    //Patient *thePatient = [patients objectAtIndex:selectedPatient];
    //NSLog(@"el paciente es: %@", thePatient);
    
    //get the master view controller and perform the appropiate segues
    id masterViewController = [self.splitViewController.viewControllers objectAtIndex:0];
    [self performSegueWithIdentifier:@"gotoHistory" sender:nil];
    
    if([masterViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController* nav = (UINavigationController*)masterViewController;
        [[nav.viewControllers objectAtIndex:0] performSegueWithIdentifier:@"gotoPatientHistory" sender:self];
    }
    //perform segue for master view controller
}

- (IBAction)showIntructions:(id)sender
{
    self.showInstructions = !showInstructions;
    
    instructions.hidden = self.showInstructions;
}

//-----------------------------------------------------------------------------------------
#pragma mark - Key-Value observing methods
-(void)handleDataModelChange:(NSNotification *)note
{
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    if(insertedObjects){
        //a sort descriptor
        NSArray* sortDescriptor = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES]];
        NSArray *theObjects = [[insertedObjects allObjects] sortedArrayUsingDescriptors:sortDescriptor];
        
        //NSLog(@"%@", theObjects);
        
        if([theObjects count] > 0){
            [self updateTileViewsWithManagedObject:theObjects inScrollView:self.todaysAppointmentsScrollView andPresentationStyle:PatientsTileViewPresentationStylePhoto];
            [self updateTileViewsWithManagedObject:theObjects inScrollView:self.futureAppointmentsScrollView andPresentationStyle:PatientsTileViewPresentationStyleSimple];
        }
        
        for (NSManagedObject *fetchedObject in theObjects) {
            if([fetchedObject isKindOfClass:[Patient class]])
            {
                [patients addObject:(Patient *)fetchedObject];
            }
        }
    }
}

@end
