//
//  MMTCSPatientInfoViewController.m
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSPatientInfoViewController.h"
#import "AccordionView.h"
#import "MMTCSGradientView.h"

#pragma mark - Private Stuff

@interface MMTCSPatientInfoViewController ()
-(void)todaysDate;

-(void)dropdownMenu;

-(void)gradientBackground;

- (UIButton *)createButtonWithFrame:(CGRect)frame withPresentationStyle:(MMTCSButtonPresentationStyle)style andTitle:(NSString *)title;

- (UILabel *)createLabelWithFrame:(CGRect)frame withPresentationStyle:(MMTCSLabelPresentationStyle)style andText:(NSString *)text;

-(UITextField *)createTextFieldWithFrame:(CGRect)frame andText:(NSString *)text;

//--------
- (void)handleDataModelChange:(NSNotification *)notifiction;

- (void)updateDropdownMenu:(NSManagedObject *)managedObject;

-(void)keyboardWillHide:(NSNotification *)note;

-(void)keyboardWillShow:(NSNotification *)note;


@end

//-----------------------------------------------------------------------
#pragma mark - static variables

static UIColor* grayColor;
static UIColor* grayishBlueColor;
static UIColor* lightGrayColor;

@implementation MMTCSPatientInfoViewController

////////////////////
//Synthesized Properties
////////////////////
#pragma mark - Synthesized Properties
@synthesize dateLabel;
@synthesize patientNameLabel;
@synthesize patientPhoto;
@synthesize gradientView;
@synthesize todaysDateString;
@synthesize accordion;
@synthesize mirrored;
@synthesize superContainerScrollView;
@synthesize detailViewController;
@synthesize managedObjectContext;
@synthesize currentPatient;
@synthesize dynamicLabels, keyboardIsShowing;
@synthesize recordButtons, currentRecordButton;
@synthesize pulseLabel,respiratoryRateLabel,bloodPressureLabel,tempetatureLabel, pronosticTextView, OxygenSaturationLabel;
@synthesize diagnosticTextView,treatmentTextView,commentsTextView;

//-----------------------------------------------------------------------
////////////////////
//Initialization Methods
////////////////////
#pragma mark - Initialization Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//--------------------------------------------------
////////////////////
//View Lifecycle
////////////////////
#pragma mark - View lifecycle

- (void)awakeFromNib
{
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.currentPatient){
        NSLog(@"%@",self.currentPatient);
        self.patientNameLabel.text = self.currentPatient.name;
    }else{
        NSLog(@"Patient not set");
    }
    
    //set the manged Object context
    MMTCSAppDelegate *mainDelegate = (MMTCSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = mainDelegate.managedObjectContext;
    
    //add a KVO observer to track changes in the CoreData Model, and handle it in a method
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(handleDataModelChange:) 
                                                 name:NSManagedObjectContextObjectsDidChangeNotification 
                                               object:self.managedObjectContext];
    //hide the back button
    self.navigationItem.hidesBackButton = YES;
    //initialize common colors
    grayColor = [UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:1];
	grayishBlueColor = [UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1];
    lightGrayColor = [UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:0.1];
    
    dynamicLabels = [NSMutableArray array];
    self.recordButtons = [NSMutableArray array];
    
    NSLog(@"%@", self.recordButtons);
    //setup the view's subviews
    [self gradientBackground];
    [self todaysDate];
    [self dropdownMenu];
    [self updateDropdownMenu:self.currentPatient];
    patientPhoto.layer.cornerRadius = 13.0;
    patientPhoto.layer.masksToBounds = YES;
    NSLog(@"%@", self.recordButtons);
    // Do any additional setup after loading the view, typically from a nib.
    self.detailViewController = (MMTCSVideoCallViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    //create some labels
    UILabel* staticHourLabel = [[self createLabelWithFrame:CGRectMake(30, 314, 150, 20) 
                                     withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                   andText:@"Hora de consulta"] autorelease];
    
    
    UILabel* staticDurationLabel = [[self createLabelWithFrame:CGRectMake(30, 340, 130, 20)
                                         withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                       andText:@"Duración"] autorelease];
    
    UILabel*  dynamicHourLabel= [[self createLabelWithFrame:CGRectMake(136, 314, 150, 20)
                                      withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                    andText:@"14:30pm" ] autorelease];
    [dynamicHourLabel setTextAlignment:UITextAlignmentRight];
    
    UILabel* dynamicDurationLabel = [[self createLabelWithFrame:CGRectMake(136, 340, 150, 20)
                                          withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                        andText:@"1 Hora" ] autorelease];
    [dynamicDurationLabel setTextAlignment:UITextAlignmentRight];
    
    
    //---------------------------------
    
    [self.view addSubview:staticHourLabel];
    [self.view addSubview:staticDurationLabel];
    [self.view addSubview:dynamicHourLabel];
    [self.view addSubview:dynamicDurationLabel];
    
    //the keyboard adaptation
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillShow:) 
                                                 name:UIKeyboardWillShowNotification 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillHide:) 
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
    
    //recognizer = [[MMTCSSpeechRecognizer alloc] init];
    //[recognizer addObserver:self forKeyPath:@"transactionState" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
}


- (void)viewDidUnload
{
    [self setDateLabel:nil];
    [self setPatientNameLabel:nil];
    [self setPatientPhoto:nil];
    [self setAccordion:nil];
    [self setGradientView:nil];
    //[self setRecordButtons:nil];
    //[self setCurrentRecordButton:nil];
    
    [self setSuperContainerScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)dealloc {
    
    [dateLabel release];
    [patientNameLabel release];
    [patientPhoto release];
    [accordion release];
    [gradientView release];
    [managedObjectContext release];
    //[recognizer removeObserver:self forKeyPath:@"transactionState"];
    //[recognizer release];
    [currentPatient release];
    //[recordButtons release];
    [currentRecordButton release];
    //[dynamicLabels release];
    
    [superContainerScrollView release];
    [super dealloc];
}

//-----------------------------------------------------------------
#pragma mark - Helper Methods

-(void)todaysDate
{
    // NSDateFormatter
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle: NSDateFormatterFullStyle];
	NSLocale *colLocate = [[NSLocale alloc]initWithLocaleIdentifier:@"es_ES"];
	[formatter setLocale:colLocate];
	//[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-18000]];
	
	//asignacion de la fecha a un string
	todaysDateString = [formatter stringFromDate:[NSDate date]];
	dateLabel.text = todaysDateString;
    
    [formatter release];
    [colLocate release];
	
	//NSLog(@"fecha = %@",fechaLabel.text);
}

-(void)dropdownMenu
{
    if ([dynamicLabels count]>0) {
        [dynamicLabels removeAllObjects];
    }
    if(recordButtons && [self.recordButtons count]>0){
        [self.recordButtons removeAllObjects];
    }
    
    
    self.accordion = [[AccordionView alloc] initWithFrame:CGRectMake(0, 370, 320, 420)];
	
	/** HEADER DATOS BIOMETRICOS **/
    UIButton* header1 = [[self createButtonWithFrame:CGRectMake(0, 0, 0, 43)
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader
                                            andTitle:@"Signos Vitales"]autorelease];
	
	UIView* viewOne = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 290)];
	[viewOne setBackgroundColor:[UIColor whiteColor]];
	
	/** labels estáticos datos biométricos**/
    UILabel* labelTemperatura       = [[self createLabelWithFrame:CGRectMake(-180, 10, 500, 55)
                                           withPresentationStyle:MMTCSLabelPresentationStyleBiometricDataStatic
                                                         andText:@"Temperatura"] autorelease];
    
    UILabel* labelRitmoCardiaco     = [[self createLabelWithFrame:CGRectMake(-170,70, 500, 55)
                                            withPresentationStyle:MMTCSLabelPresentationStyleBiometricDataStatic 
                                                          andText:@"Ritmo Cardiaco"] autorelease];
    UILabel* labelPresionArterial   = [[self createLabelWithFrame:CGRectMake(-170, 130, 500, 55)
                                            withPresentationStyle:MMTCSLabelPresentationStyleBiometricDataStatic
                                                          andText:@"Presión Arterial"] autorelease];     
    UILabel* labelRitmoRespiratorio = [[self createLabelWithFrame:CGRectMake(-160, 190, 500, 55)
                                            withPresentationStyle:MMTCSLabelPresentationStyleBiometricDataStatic 
                                                          andText:@"Ritmo Respiratorio"] autorelease]; 
    UILabel* labelSaturacionOxigeno = [[self createLabelWithFrame:CGRectMake(-160, 250, 500, 55)
                                            withPresentationStyle:MMTCSLabelPresentationStyleBiometricDataStatic 
                                                          andText:@"Saturación de Oxigeno"] autorelease]; 
	
	/** labels dinámicos datos biometricos**/
    UITextField* labelDinamicoTemperatura = [self createTextFieldWithFrame:CGRectMake(172, 15, 130, 40)
                                                           andText:[NSString stringWithFormat:@"%d ºC", 37.5]];
    labelDinamicoTemperatura.keyboardType = UIKeyboardTypeNumberPad;
    
    self.tempetatureLabel = labelDinamicoTemperatura;
    
    UITextField* labelDinamicoRitmoCardiaco = [self createTextFieldWithFrame:CGRectMake(172, 75, 130, 40)
                                                             andText:[NSString stringWithFormat:@"%d LPM",80.0]];
    labelDinamicoRitmoCardiaco .keyboardType = UIKeyboardTypeNumberPad;
    self.pulseLabel = labelDinamicoRitmoCardiaco;
    
    UITextField* labelDinamicoPresionArterial = [self createTextFieldWithFrame:CGRectMake(172, 135, 130, 40)
                                                               andText:[NSString stringWithFormat:@"%d/%d", 130, 80]];
    labelDinamicoPresionArterial.keyboardType = UIKeyboardTypeNumberPad;
    self.bloodPressureLabel = labelDinamicoPresionArterial;
    
    UITextField* labelDinamicoRitmoRespiratorio = [self createTextFieldWithFrame:CGRectMake(172, 195, 130, 40)
                                                                 andText:[NSString stringWithFormat:@"%d RPM", 26.0]];
    labelDinamicoRitmoRespiratorio.keyboardType = UIKeyboardTypeNumberPad;
    self.respiratoryRateLabel = labelDinamicoRitmoRespiratorio;
    
    UITextField* labelDinamicoSaturacionOxigeno = [self createTextFieldWithFrame:CGRectMake(172, 255, 130, 40)
                                                                         andText:[NSString stringWithFormat:@"%d%", 26.0]];
    labelDinamicoSaturacionOxigeno.keyboardType = UIKeyboardTypeNumberPad;
    self.OxygenSaturationLabel = labelDinamicoSaturacionOxigeno;
    
    [viewOne addSubview:labelTemperatura];
	[viewOne addSubview:labelRitmoCardiaco];
    [viewOne addSubview:labelPresionArterial];
    [viewOne addSubview:labelRitmoRespiratorio];
    [viewOne addSubview:labelSaturacionOxigeno];
    [viewOne addSubview:self.tempetatureLabel];
    [viewOne addSubview:self.pulseLabel];
	[viewOne addSubview:self.bloodPressureLabel];
    [viewOne addSubview:self.respiratoryRateLabel];
    [viewOne addSubview:self.OxygenSaturationLabel];
    
    /*
    UIButton * temperaturaRecordButton = [[self createButtonWithFrame:CGRectMake(260, 15, 40, 40)
                                               withPresentationStyle:MMTCSButtonPresentationStyleRecord
                                                            andTitle:nil] autorelease];
    [temperaturaRecordButton setTag:0];
    [temperaturaRecordButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
    [recordButtons addObject:temperaturaRecordButton];
    
    UIButton * ritmoCardiacoRecordButton = [[self createButtonWithFrame:CGRectMake(260, 75, 40, 40) 
                                                  withPresentationStyle:MMTCSButtonPresentationStyleRecord
                                                               andTitle:nil] autorelease];
    [ritmoCardiacoRecordButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
    [ritmoCardiacoRecordButton setTag:1];
    [recordButtons addObject:ritmoCardiacoRecordButton];
    
    UIButton * presionArterialRecordButton = [[self createButtonWithFrame:CGRectMake(260, 135, 40, 40) 
                                                    withPresentationStyle:MMTCSButtonPresentationStyleRecord 
                                                                 andTitle:nil] autorelease];
    [presionArterialRecordButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
    [presionArterialRecordButton setTag:2];
    [recordButtons addObject:presionArterialRecordButton];
    
    UIButton * ritmoRespiratorioRecordButton = [[self createButtonWithFrame:CGRectMake(260, 195, 40, 40)
                                                      withPresentationStyle:MMTCSButtonPresentationStyleRecord
                                                                   andTitle:nil] autorelease];
    [ritmoRespiratorioRecordButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
    [ritmoRespiratorioRecordButton setTag:3];
    [recordButtons addObject:ritmoRespiratorioRecordButton];
    
    [viewOne addSubview:[recordButtons objectAtIndex:0]];
    [viewOne addSubview:[recordButtons objectAtIndex:1]];
    [viewOne addSubview:[recordButtons objectAtIndex:2]];
    [viewOne addSubview:[recordButtons objectAtIndex:3]];
    */
	[self.accordion addHeader:header1 withView:viewOne];
    
	
	/** HEADER INFORMACIÓN PERSONAL  **/
	
	
    UIButton* header2 = [[self createButtonWithFrame:CGRectMake(0, 0, 0, 43) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Información Personal"] autorelease];
	
	UIView* viewTwo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewTwo setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos información personal **/
    UILabel* labelNombre    = [[self createLabelWithFrame:CGRectMake(30, 30, 100, 20) 
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Nombre"] autorelease];
    UILabel* labelEdad      = [[self createLabelWithFrame:CGRectMake(30, 60, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Edad"] autorelease];
    UILabel* labelTelefono  = [[self createLabelWithFrame:CGRectMake(30, 90, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Teléfono de Contacto"] autorelease];
    UILabel* labelDireccion = [[self createLabelWithFrame:CGRectMake(30, 120, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Dirección"] autorelease];

	
	/** labels dinámicos información personal **/
    UILabel* labelDinamicoNombre = [[self createLabelWithFrame:CGRectMake(170, 30, 150, 20)
                                         withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                       andText:@"Andrea Hernandez"] autorelease];
    [dynamicLabels addObject:labelDinamicoNombre];
    
    UILabel* labelDinamicoEdad = [[self createLabelWithFrame:CGRectMake(170, 60, 100, 20)
                                       withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                     andText:@"45"] autorelease];
    [dynamicLabels addObject:labelDinamicoEdad];
    
    UILabel* labelDinamicoTelefono = [[self createLabelWithFrame:CGRectMake(170, 90, 100, 20)
                                           withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                         andText:@"5543221"] autorelease];
    [dynamicLabels addObject:labelDinamicoTelefono];
    
    UILabel* labelDinamicoDireccion = [[self createLabelWithFrame:CGRectMake(170, 120, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                          andText:@"calle 1a #30 40"] autorelease];
    [dynamicLabels addObject:labelDinamicoDireccion];
    
	
	[viewTwo addSubview:labelNombre];
    [viewTwo addSubview:labelEdad];
    [viewTwo addSubview:labelTelefono]; 
    [viewTwo addSubview:labelDireccion];
    [viewTwo addSubview:[dynamicLabels objectAtIndex:0]];
    [viewTwo addSubview:[dynamicLabels objectAtIndex:1]];
	[viewTwo addSubview:[dynamicLabels objectAtIndex:2]]; 
    [viewTwo addSubview:[dynamicLabels objectAtIndex:3]];
	[self.accordion addHeader:header2 withView:viewTwo];
    
    [viewTwo release];
	//---------------------
    
    /** HEADER INFO HISTORIA CLINICA **/
    UIButton* header4 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Historia Clínica"] autorelease];
    
	UIView* viewFour = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewFour setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(30, 30, 240, 190)];
    [textView setText:@""];
    [textView setFont:[UIFont boldSystemFontOfSize:13]];
    [textView setTextColor:grayColor];
    [dynamicLabels addObject:textView];
    
	[viewFour addSubview:[dynamicLabels objectAtIndex:4]];
    
    [self.accordion addHeader:header4 withView:viewFour];
    
    
    //---------------------
    
    /** HEADER INFO HISTORIA CLINICA **/
    UIButton* header5 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Diagnóstico"] autorelease];
    
	UIView* viewFive = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewFive setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    textView = [[UITextView alloc] initWithFrame:CGRectMake(30, 30, 240, 190)];
    [textView setText:@"inserte su diagnóstico aquí..."];
    [textView setFont:[UIFont boldSystemFontOfSize:13]];
    [textView setTextColor:grayColor];
    [dynamicLabels addObject:textView];
    
	[viewFive addSubview:[dynamicLabels objectAtIndex:5]];
    
    [self.accordion addHeader:header5 withView:viewFive];

    //---------------------
    
    /** HEADER INFO HISTORIA CLINICA **/
    UIButton* header6 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Pronóstico"] autorelease];
    
	UIView* viewSix = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewSix setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    textView = [[UITextView alloc] initWithFrame:CGRectMake(30, 30, 240, 190)];
    [textView setText:@"inserte el pronostico que usted tiene aquí..."];
    [textView setFont:[UIFont boldSystemFontOfSize:13]];
    [textView setTextColor:grayColor];
    [dynamicLabels addObject:textView];
    
	[viewSix addSubview:[dynamicLabels objectAtIndex:6]];
    [self.accordion addHeader:header6 withView:viewSix];
    
    //---------------------
    
    /** HEADER INFO HISTORIA CLINICA **/
    UIButton* header7 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Tratamiento"] autorelease];
    
	UIView* viewSeven = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewSeven setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    textView = [[UITextView alloc] initWithFrame:CGRectMake(30, 30, 240, 190)];
    [textView setText:@"describa cómo va a tratar al paciente aquí..."];
    [textView setFont:[UIFont boldSystemFontOfSize:13]];
    [textView setTextColor:grayColor];
    [dynamicLabels addObject:textView];
    
	[viewSeven addSubview:[dynamicLabels objectAtIndex:7]];
    
    [self.accordion addHeader:header7 withView:viewSeven];
    
    //---------------------
    
    /** HEADER INFO HISTORIA CLINICA **/
    UIButton* header8 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Comentarios"] autorelease];
    
	UIView* viewEight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewEight setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    textView = [[UITextView alloc] initWithFrame:CGRectMake(30, 30, 240, 190)];
    [textView setText:@"escriba información de remisión del paciente, evolución, etc. aquí..."];
    [textView setFont:[UIFont boldSystemFontOfSize:13]];
    [textView setTextColor:grayColor];
    [dynamicLabels addObject:textView];
    
	[viewEight addSubview:[dynamicLabels objectAtIndex:8]];
    
    [self.accordion addHeader:header8 withView:viewEight];
    
    //[viewEight release];
	
	[self.accordion setNeedsLayout];
	[self.accordion setAllowsMultipleSelection:YES];	
	[self.view addSubview:self.accordion];
}

-(void)updateDropdownMenu:(NSManagedObject *)managedObject
{
    if([managedObject isKindOfClass:[Patient class]]){
        //de 4 a 7
        Patient *patient = (Patient *)managedObject;
        
        [[dynamicLabels objectAtIndex:0] setText:patient.name];
        [[dynamicLabels objectAtIndex:1] setText:[patient.age stringValue]];
        [[dynamicLabels objectAtIndex:2] setText:patient.home_phone];
        [[dynamicLabels objectAtIndex:3] setText:patient.address];
        [[dynamicLabels objectAtIndex:4] setText:patient.history];
        //[[dynamicLabels objectAtIndex:5] setText:patient.history];
        [self.patientPhoto setImage:[UIImage imageWithContentsOfFile:patient.photo]];
        
    }
    else if([managedObject isKindOfClass:[Consultation class]]){
        //de 0 a 3
        Consultation *consultation = (Consultation *)managedObject;
        
        NSString *temperature = [NSString stringWithFormat:@"%d ºC", consultation.temperature];
        [self.tempetatureLabel setText:temperature];
        
        NSString *pulse = [NSString stringWithFormat:@"%d Cs", consultation.pulse];
        [self.pulseLabel setText:pulse];
        
        NSString *blood_pressure = [NSString stringWithFormat:@"%d mmHg", consultation.blood_pressure];
        [self.bloodPressureLabel setText:blood_pressure];
        
        NSString *respiratory_rate = [NSString stringWithFormat:@"%d mmHg", consultation.respiratory_rate];
        [self.respiratoryRateLabel setText:respiratory_rate];
    }
}

-(void)gradientBackground
{
    self.gradientView = [[MMTCSGradientView alloc] init];
	[self.view addSubview:self.gradientView];
	[self.view sendSubviewToBack:self.gradientView];
}



//----------------------------------------------------------------
#pragma mark - UIHelper Methods

-(UIButton *)createButtonWithFrame:(CGRect)frame withPresentationStyle:(MMTCSButtonPresentationStyle)style andTitle:(NSString *)title
{
    UIButton* theButton = [[[UIButton alloc] initWithFrame:frame] autorelease];
    
    switch (style) {
        case MMTCSButtonPresentationStyleAccordionHeader:
            [theButton setTitle:title forState:UIControlStateNormal];
            //color and image styles
            [theButton setTitleColor:grayishBlueColor forState:UIControlStateNormal];
            [theButton setBackgroundImage:[UIImage imageNamed:@"tab_bg.png"] forState:UIControlStateNormal];
            //title label styles
            [theButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
            [theButton.titleLabel setShadowColor:[UIColor blackColor]];
            [theButton.titleLabel setShadowOffset:CGSizeMake(0, 1)];
            break;
        
        case MMTCSButtonPresentationStyleRecord:
            [theButton setTitle:title forState:UIControlStateNormal];
            [theButton setBackgroundImage:[UIImage imageNamed:@"record_button.png"] forState:UIControlStateNormal];
            [theButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        default:
            break;
    }
    
    return theButton;
}

-(UILabel *)createLabelWithFrame:(CGRect)frame withPresentationStyle:(MMTCSLabelPresentationStyle)style andText:(NSString *)text
{
    UILabel *theLabel = [[UILabel alloc] initWithFrame:frame];
    [theLabel setText:text];
    
    switch (style) {
        case MMTCSLabelPresentationStyleStatic:
            [theLabel setTextColor:grayishBlueColor]; 
            theLabel.font = [UIFont boldSystemFontOfSize:13];
            theLabel.opaque = NO; 
            theLabel.backgroundColor = [UIColor clearColor];
            break;
            
        case MMTCSLabelPresentationStyleDynamic:
            [theLabel setTextColor:grayColor];
            theLabel.font = [UIFont boldSystemFontOfSize:13];
            theLabel.opaque = NO; 
            theLabel.backgroundColor = [UIColor clearColor];
            break;
        
        case MMTCSLabelPresentationStyleBiometricDataStatic:
            [theLabel setTextColor:grayishBlueColor]; 
            theLabel.font = [UIFont boldSystemFontOfSize:14];
            theLabel.backgroundColor = lightGrayColor;
            theLabel.textAlignment = UITextAlignmentCenter;
            break;
            
        case MMTCSLabelPresentationStyleBiometricDataDynamic:
            theLabel.opaque = NO;
            theLabel.backgroundColor = [UIColor clearColor];
            [theLabel setTextColor:grayColor]; 
            theLabel.font = [UIFont boldSystemFontOfSize:28];
            break;
        default:
            break;
    }
    
    return theLabel;
}

-(UITextField *)createTextFieldWithFrame:(CGRect)frame andText:(NSString *)text
{
    UITextField *theTextfield = [[UITextField alloc] initWithFrame:frame];
    [theTextfield setText:text];
    
    theTextfield.opaque = NO;
    theTextfield.backgroundColor = [UIColor clearColor];
    [theTextfield setTextColor:grayColor]; 
    theTextfield.font = [UIFont boldSystemFontOfSize:28];
    
    return theTextfield;
}
//----------------------------------------------------------------
#pragma mark - Key-Value Observing methods

- (void)handleDataModelChange:(NSNotification *)notifiction
{
    
}


//----------------------------------------------------------------
#pragma mark - Action Methods

-(IBAction)startRecord:(id)sender
{
    NSLog(@"Hola!%@",self.recordButtons);
    self.currentRecordButton = (UIButton *)sender;
    //[recognizer connect];
    //[recognizer startSpeechRecognizerWithType:SKDictationRecognizerType 
    //                                detection:SKLongEndOfSpeechDetection
    //                                 language:@"es_ES"];
}

//----------------------------------------------------------------
#pragma mark - Key Value Observing methods

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"transaction state changed");
    if ([keyPath isEqual:@"transactionState"]) {
        NSNumber* newState = [change objectForKey:NSKeyValueChangeNewKey];
        NSLog(@"%@, class:%@",newState, NSStringFromClass([newState class]));
        
        switch ([newState intValue]) {
            case MMTCSSpeechRecognizerTransactionStateRecording:
                [self.currentRecordButton setBackgroundImage:[UIImage imageNamed:@"record_button_pressed.png"] forState:UIControlStateNormal];
                break;
            
            case MMTCSSpeechRecognizerTransactionStateIdle:
                NSLog(@"change button!");
                [self.currentRecordButton setBackgroundImage:[UIImage imageNamed:@"record_button.png"] forState:UIControlStateNormal];
                NSInteger index = self.currentRecordButton.tag;
                NSString *units=@"";
                NSString *result = [recognizer.results objectAtIndex:0];
                NSLog(@"%@",result);
                
                switch (index) {
                    case 0:
                        units = @"ºC";
                        if([result isNumeric]){
                            NSString *formatted = [NSString stringWithFormat:@"%@ %@",result,units];
                            [self.tempetatureLabel setText:formatted];
                        }
                        break;
                    case 1:
                        units = @"Cs";
                        if([result isNumeric]){
                            NSString *formatted = [NSString stringWithFormat:@"%@ %@",result,units];
                            [self.pulseLabel setText:formatted];
                        }
                        break;
                    case 2:
                        units = @"mmHg";
                        if([result isNumeric]){
                            NSString *formatted = [NSString stringWithFormat:@"%@ %@",result,units];
                            [self.bloodPressureLabel setText:formatted];
                        }
                        break;
                    case 3:
                        units = @"Fr";
                        if([result isNumeric]){
                            NSString *formatted = [NSString stringWithFormat:@"%@ %@",result,units];
                            [self.respiratoryRateLabel setText:formatted];
                        }
                        break;

                        
                    default:
                        break;
                }
                break;
                
            default:
                break;
        }         
    }
}

//---------

#pragma mark - UITextFieldDelegate Methods
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString *newValue;
    if ([textField isEqual:self.tempetatureLabel]) {
        NSString *value = textField.text;
        newValue = [NSString stringWithFormat:@"%d ºC", value];
    }
    if ([textField isEqual:self.pulseLabel]) {
        NSString *value = textField.text;
        newValue = [NSString stringWithFormat:@"%d LPM", value];
    }
    if ([textField isEqual:self.respiratoryRateLabel]) {
        NSString *value = textField.text;
        newValue = [NSString stringWithFormat:@"%d RMP", value];
    }
    if ([textField isEqual:self.OxygenSaturationLabel]) {
        NSString *value = textField.text;
        newValue = [NSString stringWithFormat:@"%d\%", value];
    }
    [textField setText:newValue];
}

//este metodo es llamado cuando se hace tap en el boton enter del teclado (o return o done o lo q sea, es siempre el mismo boton)
- (BOOL) textFieldShouldReturn:(UITextField *)theTextField
{
    [theTextField resignFirstResponder];
    NSString *newValue;
    if ([theTextField isEqual:self.tempetatureLabel]) {
        NSString *value = theTextField.text;
        newValue = [NSString stringWithFormat:@"%d ºC", value];
    }
    if ([theTextField isEqual:self.pulseLabel]) {
        NSString *value = theTextField.text;
        newValue = [NSString stringWithFormat:@"%d LPM", value];
    }
    if ([theTextField isEqual:self.respiratoryRateLabel]) {
        NSString *value = theTextField.text;
        newValue = [NSString stringWithFormat:@"%d RMP", value];
    }
    if ([theTextField isEqual:self.OxygenSaturationLabel]) {
        NSString *value = theTextField.text;
        newValue = [NSString stringWithFormat:@"%d\%", value];
    }
    [theTextField setText:newValue];
    return YES;
}

-(void)keyboardWillShow:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.view.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view 
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.origin.y -= keyboardBounds.size.height;
    else 
        frame.origin.y -= keyboardBounds.size.width;
    
    // Apply new size of table view
    self.view.frame = frame;
    
    [UIView commitAnimations];
}


-(void)keyboardWillHide:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    //NSLog(@"%@",keyboardBounds);
    CGRect frame = self.view.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view 
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.origin.y += keyboardBounds.size.height;
    else 
        frame.origin.y += keyboardBounds.size.width;
    
    // Apply new size of table view
    self.view.frame = frame;
    
    [UIView commitAnimations];
}

@end
