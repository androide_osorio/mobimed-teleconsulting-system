//
//  MMTCSSpeechRecognizer.m
//  Mobimed
//
//  Created by evento on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSSpeechRecognizer.h"

//the credentials
const unsigned char SpeechKitApplicationKey[] = {0xc9, 0x62, 0x00, 0xe6, 0x80, 0xc3, 0x51, 0x55, 0x43, 0x77, 0x3d, 0x19, 0x16, 0x28, 0xb6, 0xd3,0xf2, 0x82, 0x03, 0x5f, 0xf0, 0x70, 0xb1, 0x0c, 0x34, 0x03, 0x87, 0x59, 0x97, 0xcb, 0xbf, 0x60, 0x2d, 0xc0, 0x46, 0xae, 0x83, 0x97, 0x03, 0xc7, 0x0e, 0x40, 0xe0, 0x25, 0x69, 0x96, 0xf8, 0x5b, 0x18, 0xfc, 0xba, 0x08, 0x6c, 0x6b, 0x15, 0xa1, 0x6f, 0x9c, 0xfa, 0x06, 0x67, 0xba, 0x66, 0x95};

NSString* const SpeechKitAppID = @"NMDPTRIAL_androide_osorio20120308152455";

NSString* const SpeechKitHost = @"sandbox.nmdp.nuancemobility.net";

const unsigned long SpeechKitPort = 443;

//
//---------------------------------------------------------------------
//

@implementation MMTCSSpeechRecognizer

@synthesize voiceSearch;
@synthesize transactionState;
@synthesize results;

-(id) init
{
    if(self = [super init]){
        
    }
    return self;
}

-(void)dealloc
{
    [voiceSearch release];
    [SpeechKit destroy];
    [super dealloc];
}

-(void) connect
{
    [SpeechKit setupWithID:SpeechKitAppID
                      host:SpeechKitHost
                      port:SpeechKitPort
                    useSSL:NO
                  delegate:self];
    
    SKEarcon* earconStart	= [SKEarcon earconWithName:@"earcon_listening.wav"];
	SKEarcon* earconStop	= [SKEarcon earconWithName:@"earcon_done_listening.wav"];
	SKEarcon* earconCancel	= [SKEarcon earconWithName:@"earcon_cancel.wav"];
	
	[SpeechKit setEarcon:earconStart forType:SKStartRecordingEarconType];
	[SpeechKit setEarcon:earconStop forType:SKStopRecordingEarconType];
	[SpeechKit setEarcon:earconCancel forType:SKCancelRecordingEarconType];
}

-(void)startSpeechRecognizerWithType:(NSString *)type detection:(SKEndOfSpeechDetection)detection language:(NSString *)language
{
    if (voiceSearch) [voiceSearch release];
    
    voiceSearch = [[SKRecognizer alloc] initWithType:type
                                           detection:detection
                                            language:language 
                                            delegate:self];

}

//------------------------------------------------------------------------------------
#pragma mark -
#pragma mark SKRecognizerDelegate methods

- (void) destroyed {
    // Debug - Uncomment this code and fill in your app ID below, and set
    // the Main Window nib to MainWindow_Debug (in DMRecognizer-Info.plist)
    // if you need the ability to change servers in DMRecognizer
    //
    [SpeechKit setupWithID:SpeechKitAppID
                      host:SpeechKitHost
                      port:SpeechKitPort
                    useSSL:NO
                  delegate:self];
    
    SKEarcon* earconStart	= [SKEarcon earconWithName:@"earcon_listening.wav"];
	SKEarcon* earconStop	= [SKEarcon earconWithName:@"earcon_done_listening.wav"];
	SKEarcon* earconCancel	= [SKEarcon earconWithName:@"earcon_cancel.wav"];
	
	[SpeechKit setEarcon:earconStart forType:SKStartRecordingEarconType];
	[SpeechKit setEarcon:earconStop forType:SKStopRecordingEarconType];
	[SpeechKit setEarcon:earconCancel forType:SKCancelRecordingEarconType];

}
/**
 *
 */
- (void)recognizerDidBeginRecording:(SKRecognizer *)recognizer
{
    NSLog(@"Recording started.");
    
    [self setTransactionState:MMTCSSpeechRecognizerTransactionStateRecording];
}

/**
 *
 */
- (void)recognizerDidFinishRecording:(SKRecognizer *)recognizer
{
    NSLog(@"Recording finished.");
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateVUMeter) object:nil];
    [self setTransactionState:MMTCSSpeechRecognizerTransactionStateProcessing];
}

/**
 *
 */
- (void)recognizer:(SKRecognizer *)recognizer didFinishWithResults:(SKRecognition *)theResults
{
    NSLog(@"Got results.");
    NSLog(@"Session id [%@].", [SpeechKit sessionID]); // for debugging purpose: printing out the speechkit session id 
    
    long numOfResults = [theResults.results count];
    
    NSLog(@"%@", theResults);
    if (numOfResults > 0)
        self.results = [NSArray arrayWithArray:theResults.results];
    
	if (numOfResults > 1) 
		self.results = [theResults.results subarrayWithRange:NSMakeRange(1, numOfResults-1)];
    
    if (theResults.suggestion) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suggestion"
                                                        message:theResults.suggestion
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];        
        [alert show];
        [alert release];
    }
    [self setTransactionState:MMTCSSpeechRecognizerTransactionStateIdle];
	[voiceSearch release];
	voiceSearch = nil;
}

/**
 *
 */
- (void)recognizer:(SKRecognizer *)recognizer didFinishWithError:(NSError *)error suggestion:(NSString *)suggestion
{
    NSLog(@"Got error.");
    NSLog(@"Session id [%@].", [SpeechKit sessionID]); // for debugging purpose: printing out the speechkit session id 
    
    [self setTransactionState:MMTCSSpeechRecognizerTransactionStateIdle];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];        
    [alert show];
    [alert release];
    
    if (suggestion) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suggestion"
                                                        message:suggestion
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];        
        [alert show];
        [alert release];
        
    }
    
	[voiceSearch release];
	voiceSearch = nil;
}

//--------------------------------------------------------------------------------------
@end
