//
//  MMTCSMedicalHistoryMasterViewController.h
//  Mobimed
//
//  Created by evento on 18/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "MMTCSMedicalHistoryDetailViewController.h"
#import "MMTCSPatientInfoViewController.h"

@interface MMTCSMedicalHistoryMasterViewController : UIViewController

@property (strong, nonatomic) MMTCSMedicalHistoryDetailViewController* detailViewController;

@property (retain, nonatomic) IBOutlet UIImageView *patientImage;

@property (retain, nonatomic) IBOutlet UILabel *patientNameLabel;

@property (nonatomic,retain) MMTCSGradientView *gradientView;

@property (nonatomic) BOOL mirrored;

@property (nonatomic, retain) AccordionView* accordion;

@property (retain, nonatomic) Patient *currentPatient;


@end
