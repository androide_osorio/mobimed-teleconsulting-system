//
//  MMTCSNetworkManager.m
//  Mobimed
//
//  Created by evento on 22/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSNetworkManager.h"

//
// Private Methods
//
@interface MMTCSNetworkManager ()

-(void) serverQueueDidStart;

-(void) loginRequestDidFail:(ASIHTTPRequest *)theRequest;

-(void) loginRequestDidFinish:(ASIHTTPRequest *)theRequest;

-(void) requestDidFinish:(ASIHTTPRequest *)theRequest;

-(void) requestDidFail:(ASIHTTPRequest *)theRequest;

-(void) imageFetchDidFinish:(ASIHTTPRequest *)theRequest;

-(void) imageFetchDidFail:(ASIHTTPRequest *)theRequest;

-(void)addNewRequestToQueue:(ASINetworkQueue *)queue withUrl:(NSURL *)url name:(NSString *)name andFilename:(NSString *)filenameOrNil;

//----------------------------------------------------------

-(void)insertNewManagedObjectFromJsonResponse:(NSString *)json;

@end

//
// SOME STRING CONSTANTS FOR THE SERVER CONNECTIONS
//
static NSString* SERVER_URL        = @"http://mobimedtest.comuf.com/";
static NSString* IMGS_PATH         = @"img";
static NSString* SERVER_LOGIN      = @"physicians/login";
static NSString* PHYSICIAN_URL     = @"physicians/findPhysicianByName";
static NSString* PATIENTS_URL      = @"patients/findPatientsByPhysicianId";
static NSString* CONSULTATIONS_URL = @"consultations/findConsultationsByPatientId";
static NSString* APPOINTMENTS_URL  = @"appointments/findAppointmentsByPhysicianId";
static NSString* ADD_APPOINT_URL   = @"appointments/addAppointment";


//------------------------------------------------------------------------------
@implementation MMTCSNetworkManager

@synthesize userLoggedIn, username, password, context;

#pragma mark - initialization Methods
-(id)init
{
    if(self = [super init]){
        imagesNetworkQueue = [[ASINetworkQueue alloc] init];
        [imagesNetworkQueue setRequestDidFinishSelector:@selector(imageFetchDidFinish:)];
        [imagesNetworkQueue setRequestDidFailSelector:@selector(imageFetchDidFail:)];
        [imagesNetworkQueue setDelegate:self];
        
        serverQueue        = [[ASINetworkQueue alloc] init];
        jsonParser         = [[SBJsonParser alloc] init];
        baseUrl = [[NSURL URLWithString:SERVER_URL] retain];
        
        self.context = [(MMTCSAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    return self;
}

-(void) loginWithUsername:(NSString *)thUsername andPassword:(NSString *)thePassword
{
    [loginRequest cancel];
    NSURL* url   = [NSURL URLWithString:SERVER_LOGIN relativeToURL:baseUrl];
    loginRequest = [ASIFormDataRequest requestWithURL:url];
    [loginRequest setDelegate:self];
    
    [loginRequest setRequestMethod:@"POST"];
	[loginRequest setPostValue:thUsername forKey:@"username"];
    self.username = thUsername;
	[loginRequest setPostValue:thePassword forKey:@"password"];
    self.password = thePassword;
    [loginRequest setDidFailSelector:@selector(loginRequestDidFail:)];
    [loginRequest setDidFinishSelector:@selector(loginRequestDidFinish:)];
	[loginRequest setTimeOutSeconds:20];
    
    [loginRequest startSynchronous];
}

-(void)requestDataFromServer
{
    [serverQueue reset];
    [serverQueue setDelegate:self];
    [serverQueue setRequestDidStartSelector:@selector(serverQueueDidStart)];
    [serverQueue setRequestDidFinishSelector:@selector(requestDidFinish:)];
    [serverQueue setRequestDidFailSelector:@selector(requestDidFail:)];
    
    //set the queue to work!
    NSURL* getPhysicianByNameURL = [[NSURL URLWithString:PHYSICIAN_URL relativeToURL:baseUrl] URLByAppendingPathComponent:self.username];
    [self addNewRequestToQueue:serverQueue
                       withUrl:getPhysicianByNameURL
                          name:@"getPhysicianByName"
                   andFilename:nil];
    
    [serverQueue go];
   // NSLog(@"Start!");
}

-(void) createFormDataRequestWithValues:(NSDictionary *)dict toURL:(NSString *)param
{
    NSURL *url;
    if([param isEqualToString:@"addAppointment"]){
        url = [NSURL URLWithString:ADD_APPOINT_URL relativeToURL:baseUrl];
    }
    ASIFormDataRequest *formData = [ASIFormDataRequest requestWithURL:url];
    [formData setDelegate:self];
    
    [formData setRequestMethod:@"POST"];
    NSArray *keys = [dict allKeys];
    for (id key in keys) {
        [formData setPostValue:(NSString *)[dict objectForKey:key] forKey:key];
    }
    [formData setDidFailSelector:@selector(requestDidFail:)];
    [formData setDidFinishSelector:@selector(requestDidFinish:)];
    [formData setTimeOutSeconds:20];
    
    [formData startAsynchronous];
}
//------------------------------------------------------------------------------------------------

-(void) serverQueueDidStart
{
    //NSLog(@"Queue did start!");
}

-(void) loginRequestDidFail:(ASIHTTPRequest *)theRequest
{
    self.userLoggedIn = NO;
}

-(void) loginRequestDidFinish:(ASIHTTPRequest *)theRequest
{
    //NSLog(@"Logged in!, %@, %@", theRequest.responseStatusCode, theRequest.responseCookies);
    self.userLoggedIn = YES;
    //NSLog(@"Logged in!");
   // NSLog(@"%@", [theRequest responseString]);
}

-(void) requestDidFinish:(ASIHTTPRequest *)theRequest
{
   // NSLog(@"user info name: %@", [theRequest.userInfo objectForKey:@"name"] );
    if([[theRequest.userInfo objectForKey:@"name"] isEqualToString:@"getPhysicianByName"]){
        //NSLog(@"%@", [theRequest responseString]);
        [self insertNewManagedObjectFromJsonResponse:[theRequest responseString]];
    }
    else if([[theRequest.userInfo objectForKey:@"name"] isEqualToString:@"findAppointmentsByPhysicianId"]){
        //NSLog(@"%@", [theRequest responseString]);
        [self insertNewManagedObjectFromJsonResponse:[theRequest responseString]];
    }
    NSLog(@"%@", [theRequest responseString]);
}

-(void) requestDidFail:(ASIHTTPRequest *)theRequest
{
    //NSLog(@"Request Failed, %@", [request error]);
}

-(void) imageFetchDidFinish:(ASIHTTPRequest *)theRequest
{
    
}

-(void) imageFetchDidFail:(ASIHTTPRequest *)theRequest
{
    
}

-(void)addNewRequestToQueue:(ASINetworkQueue *)queue withUrl:(NSURL *)url name:(NSString *)name andFilename:(NSString *)filenameOrNil;
{
    //create the HTTP request
	ASIHTTPRequest* theRequest = [ASIHTTPRequest requestWithURL:url];
	//setup the request
	//set the download destination path to the documents folder in the app
	//put all the downloaded files there!
    if(filenameOrNil){
	[theRequest setDownloadDestinationPath:
	 [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] 
	  stringByAppendingPathComponent:filenameOrNil]];
    }
    
	//each individual request in the Queue can have its own progress Delegate (luego!)
	[theRequest setUserInfo:[NSDictionary dictionaryWithObject:name forKey:@"name"]];
	//add the request to the operation
	[queue addOperation:theRequest];
	//[request release];
}


//-----------------------------------------------------------------------------------
#pragma mark - Core Data utilities Methods

-(void)insertNewManagedObjectFromJsonResponse:(NSString *)json
{
    NSManagedObject *obj;
    NSArray* response = [jsonParser objectWithString:json];
    //NSLog(@"%@", response);
    
    for (NSDictionary *object in response) {
        NSArray *keys = [object allKeys];
        
        for (NSString *key in keys) {
            NSDictionary* currentObject = [object objectForKey:key];
            NSString *downloadPath;
            
            //if it has a photo field, download the image
            if ([currentObject objectForKey:@"photo"]) {
                NSString *photoName    = [currentObject objectForKey:@"photo"];
                downloadPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:photoName];
                [self addNewRequestToQueue:imagesNetworkQueue 
                                   withUrl:[[NSURL URLWithString:IMGS_PATH relativeToURL:baseUrl] URLByAppendingPathComponent:photoName]
                                      name:photoName 
                               andFilename:photoName];
                
            }
            [imagesNetworkQueue go];
            
            //instantiate the correct NSManagedObject class
            if([key isEqualToString:@"Physician"]){
                //get the class of the next object in the dictionary
                NSString *class = [currentObject objectForKey:@"class"];
                //if it is a physician, insert an object for the netity physician
                obj =[NSEntityDescription insertNewObjectForEntityForName:class inManagedObjectContext:self.context];
                
                [obj populateFromDictionary:[object objectForKey:class]];
                [obj setValue:downloadPath forKey:@"photo"];
                
                [serverQueue reset];
                [serverQueue setDelegate:self];
                [serverQueue setRequestDidStartSelector:@selector(serverQueueDidStart)];
                [serverQueue setRequestDidFinishSelector:@selector(requestDidFinish:)];
                [serverQueue setRequestDidFailSelector:@selector(requestDidFail:)];
                
                [self addNewRequestToQueue:serverQueue
                                   withUrl:[[NSURL URLWithString:APPOINTMENTS_URL relativeToURL:baseUrl] URLByAppendingPathComponent:[currentObject valueForKey:@"id"]]
                                      name:@"findAppointmentsByPhysicianId" 
                               andFilename:nil];
                [serverQueue go];
                // NSLog(@"the core data object is: %@", obj);
            }
            else if([key isEqualToString:@"Patient"]){
                //get the class of the next object in the dictionary
                NSString *class = [currentObject objectForKey:@"class"];

                obj =[NSEntityDescription insertNewObjectForEntityForName:class inManagedObjectContext:self.context];
                
                [obj populateFromDictionary:[object objectForKey:@"Patient"]];
                // NSLog(@"the core data object is: %@", obj);
            }
            else if([key isEqualToString:@"Appointment"]){
                //get the class of the next object in the dictionary
                NSString *class = [currentObject objectForKey:@"class"];
                //if it is a physician, insert an object for the netity physician
                obj =[NSEntityDescription insertNewObjectForEntityForName:class inManagedObjectContext:self.context];
                [obj populateFromDictionary:[object objectForKey:class]];
                
                if([currentObject objectForKey:@"Patient"]){
                    if ([[currentObject objectForKey:@"Patient"] objectForKey:@"photo"]) {
                        NSString *photoName    = [[currentObject objectForKey:@"Patient"] objectForKey:@"photo"];
                        downloadPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:photoName];
                        [self addNewRequestToQueue:imagesNetworkQueue 
                                           withUrl:[[NSURL URLWithString:IMGS_PATH relativeToURL:baseUrl] URLByAppendingPathComponent:photoName]
                                              name:photoName 
                                       andFilename:photoName];
                        [[currentObject valueForKey:@"Patient"] setValue:downloadPath forKey:@"photo"];
                        [[obj valueForKey:@"Patient"] setValue:downloadPath forKey:@"photo"];
                    }
                }
            }
        } 
    }
}

@end
