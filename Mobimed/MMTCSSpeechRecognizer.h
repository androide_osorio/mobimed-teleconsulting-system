//
//  MMTCSSpeechRecognizer.h
//  Mobimed
//
//  Created by evento on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpeechKit/SpeechKit.h>

typedef enum _transactionState{
    MMTCSSpeechRecognizerTransactionStateIdle = 0,
    MMTCSSpeechRecognizerTransactionStateInitial,
    MMTCSSpeechRecognizerTransactionStateRecording,
    MMTCSSpeechRecognizerTransactionStateProcessing,
} MMTCSSpeechRecognizerTransactionState;

@interface MMTCSSpeechRecognizer : NSObject<SKRecognizerDelegate, SpeechKitDelegate>
{
    SKRecognizer* voiceSearch;
}

@property (readonly) SKRecognizer* voiceSearch;

@property MMTCSSpeechRecognizerTransactionState transactionState;

@property (retain, nonatomic) NSArray* results;

//-------------------------------------------
#pragma mark - Public Methods

- (void) connect;

- (void)startSpeechRecognizerWithType:(NSString *)type detection:(SKEndOfSpeechDetection)detection language:(NSString *)language;

@end
