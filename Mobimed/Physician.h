//
//  Physician.h
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface Physician : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSString * cellphone;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSDate * last_entry;
@property (nonatomic, retain) NSString * mayor;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * passcode;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSString * speciality;
@property (nonatomic, retain) NSSet *patient;
@end

@interface Physician (CoreDataGeneratedAccessors)

- (void)addPatientObject:(Patient *)value;
- (void)removePatientObject:(Patient *)value;
- (void)addPatient:(NSSet *)values;
- (void)removePatient:(NSSet *)values;

@end
