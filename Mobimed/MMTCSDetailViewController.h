//
//  MMTCSDetailViewController.h
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMTCSLoginViewController.h"
#import "MMTCSAddAppointmentViewController.h"
#import "MMTCSAddPatientViewController.h"

@interface MMTCSDetailViewController : UIViewController <UISplitViewControllerDelegate, MMTCSLoginViewControllerDelegate, MMTCSAddAppointmentViewControllerDelegate, MMTCSAddPatientViewControllerDelegate, UIActionSheetDelegate>

#pragma mark - properties

@property (strong, nonatomic) id detailItem;

@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@property (retain, nonatomic) IBOutlet UIScrollView *todaysAppointmentsScrollView;

@property (retain, nonatomic) IBOutlet UIScrollView *futureAppointmentsScrollView;

@property (retain, nonatomic) IBOutlet UILabel *todaysAppointmentsLabel;

@property (retain, nonatomic) IBOutlet UILabel *futureAppointmentsLabel;

@property (retain, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (retain, nonatomic) NSMutableArray *patients;

@property (assign, nonatomic) NSInteger selectedPatient;

@property BOOL showInstructions;
//---------------------------------------------------------------

#pragma mark - methods
-(IBAction)tapAreaSensible:(id)sender;

-(IBAction)tapFutureAppointment:(id)sender;

- (IBAction)showIntructions:(id)sender;

@end

