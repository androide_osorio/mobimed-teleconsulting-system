//
//  MMTCSMiniClinicalHistoryViewController.m
//  Mobimed
//
//  Created by evento on 13/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSMiniClinicalHistoryViewController.h"

static NSString* const HISTORY_CONSULTATIONS_URL = @"http://mobimedtest.comuf.com/consultations/consultationsHistoryByPatientId";

@implementation MMTCSMiniClinicalHistoryViewController

@synthesize currentPatient;
@synthesize historyWebView;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.currentPatient){
        NSString* destinationUrl = [HISTORY_CONSULTATIONS_URL stringByAppendingPathComponent:[self.currentPatient.id stringValue]];
        
        NSURL *url = [NSURL URLWithString:destinationUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.historyWebView loadRequest:request];
    }
}


- (void)viewDidUnload
{
    [self setHistoryWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)dealloc {
    [historyWebView release];
    [currentPatient release];
    [delegate release];
    [super dealloc];
}
- (IBAction)dismiss:(id)sender {
    [delegate dismissMiniClinicalHistory:self];
}
@end
