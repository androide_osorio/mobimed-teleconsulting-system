//
//  MMTCSMasterViewController.m
//  Mobimed
//
//  Created by Androide Osorio on 9/05/12.
//  Copyright (c) 2012 Androide Osorio, sebaZ. All rights reserved.
//

#import "MMTCSMasterViewController.h"
#import "MMTCSDetailViewController.h"
#import "AccordionView.h"
#import "MMTCSGradientView.h"
#import "MMTCSPatientInfoViewController.h"
#import "MMTCSMedicalHistoryMasterViewController.h"
//
//
// PRIVATE STUFF
//
//

#pragma mark - private methods category declaration
//private stuff
@interface MMTCSMasterViewController ()

- (UIButton *)createAccordionHeaderButtonWithFrame:(CGRect)frame andTitle:(NSString *)title;

- (UILabel *)createTitleLabelWithFrame:(CGRect)frame andText:(NSString *)text;

- (UILabel *)createDescriptionLabelWithFrame:(CGRect)frame andText:(NSString *)text;

-(void)handleDataModelChange:(NSNotification *)note;

-(void)updateInfoDropdown:(NSManagedObject *)mangedObject;
@end


//
// IMPLEMENTATION
//

@implementation MMTCSMasterViewController

#pragma mark - Synthesized Properties
@synthesize dateLabel;
@synthesize physicianPhoto;
@synthesize physicianNameLabel;
@synthesize dynamicLabels;
@synthesize loggedPhysician;

@synthesize detailViewController = _detailViewController;
@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize tableView;
@synthesize gradientView, mirrored, todaysDateString, accordion;

//--------------------------------------------------------------------
#pragma mark - Memory Management and multitasking
- (void)awakeFromNib
{
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 700.0);
    [super awakeFromNib];
}

- (void)dealloc
{
    [_detailViewController release];
    [__fetchedResultsController release];
    [__managedObjectContext release];
    [dateLabel release];
    [physicianPhoto release];
    [physicianNameLabel release];
    [dynamicLabels release];
    [loggedPhysician release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.detailViewController = (MMTCSDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    // Set up the edit and add buttons.
    
    //------------------------------------
    
    [self todaysDate];
	[self dropdownMenu];
    //update the information if there is a physician set in Core Data
    if(self.loggedPhysician){
        [self updateInfoDropdown:self.loggedPhysician];
    }
    
	[self gradientBackground];
    //add a KVO observer to track changes in the CoreData Model, and handle it in a method
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(handleDataModelChange:) 
                                                 name:NSManagedObjectContextObjectsDidChangeNotification 
                                               object:self.managedObjectContext];
    
    physicianPhoto.layer.cornerRadius = 13.0;
    physicianPhoto.layer.masksToBounds = YES;
}

- (void)viewDidUnload
{
    [self setDateLabel:nil];
    [self setPhysicianPhoto:nil];
    [self setPhysicianNameLabel:nil];
    [self setDynamicLabels:nil];
    //[self setLoggedPhysician:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255 blue:85.0/255 alpha:1];

    if(self.loggedPhysician){
        [self updateInfoDropdown:self.loggedPhysician];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

//------------------------------------------------------------------------------------------

#pragma mark - KVO Core Data methods


-(void)handleDataModelChange:(NSNotification *)note
{
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    NSArray *theObjects = [insertedObjects allObjects];
    if([[theObjects objectAtIndex:0] isKindOfClass:[Physician class]]){
        self.loggedPhysician = [theObjects objectAtIndex:0];
        self.physicianNameLabel.text = [NSString stringWithFormat:@"Dr. %@",self.loggedPhysician.name];
        [self updateInfoDropdown:[theObjects objectAtIndex:0]];
    }
    //NSLog(@"The set of objects that has been inserted: %@", insertedObjects);
}

//------------------------------------------------------------------------------------------
#pragma mark - Custom Methods

//get the current date (today) and show it in the label
- (void)todaysDate{
	// NSDateFormatter
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle: NSDateFormatterFullStyle];
	NSLocale *colLocate = [[NSLocale alloc]initWithLocaleIdentifier:@"es_ES"];
	[formatter setLocale:colLocate];
    [colLocate release];
	
	//asignacion de la fecha a un string
	todaysDateString = [formatter stringFromDate:[NSDate date]];
    [formatter release];
	self.dateLabel.text = todaysDateString;
	
	//NSLog(@"fecha = %@",self.dateLabel.text);
}

/**
 * Setup the dropdown menu
 */
- (void)dropdownMenu{
    self.dynamicLabels = [NSMutableArray array];
	//create the actual AccordionView
	self.accordion = [[AccordionView alloc] initWithFrame:CGRectMake(0, 300, 320, 420)];
	
    //Create the first Header
    UIButton *headerOne = [self createAccordionHeaderButtonWithFrame:CGRectMake(0, 0, 0, 43) andTitle:@"Información general"];

	
	/**
     * Create the first section's contents
     */
	UIView* viewOne = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewOne setBackgroundColor:[UIColor whiteColor]];
 
	//el contenido del header1
	/** LABELS ESTÁTICOS **/
    UILabel* labelOne = [self createTitleLabelWithFrame:CGRectMake(30, 30, 100, 20) andText:@"Edad"];   
	UILabel* labelTwo = [self createTitleLabelWithFrame:CGRectMake(30, 60, 100, 20) andText:@"Especialidad"];   
	UILabel* labelThree = [self createTitleLabelWithFrame:CGRectMake(30, 90, 100, 20) andText:@"Teléfono"];   
	UILabel* labelFour = [self createTitleLabelWithFrame:CGRectMake(30, 120, 100, 20) andText:@"Celular"];  
	UILabel* labelFive = [self createTitleLabelWithFrame:CGRectMake(30, 150, 100, 20) andText:@"Dirección"]; 
	
    	/** LABELS DINÀMICOS **/

        /** LABELS DINÀMICOS **/
    if (self.loggedPhysician) {
        UILabel* labelSix = [self createDescriptionLabelWithFrame:CGRectMake(170, 30, 180, 20) andText:[self.loggedPhysician.age stringValue]];
        [dynamicLabels addObject:labelSix];
        UILabel* labelSeven = [self createDescriptionLabelWithFrame:CGRectMake(170, 60, 180, 20) andText:self.loggedPhysician.speciality];
        [dynamicLabels addObject:labelSeven];
        UILabel* labelEight = [self createDescriptionLabelWithFrame:CGRectMake(170, 90, 180, 20) andText:self.loggedPhysician.phone];
        [dynamicLabels addObject:labelEight];
        UILabel* labelNine = [self createDescriptionLabelWithFrame:CGRectMake(170, 120, 180, 20) andText:self.loggedPhysician.cellphone];
        [dynamicLabels addObject:labelNine];
        UILabel* labelTen = [self createDescriptionLabelWithFrame:CGRectMake(170, 150, 180, 20) andText:self.loggedPhysician.address];
        [dynamicLabels addObject:labelTen];
        //NSLog(@"Labels : %@", dynamicLabels);
        
        [viewOne addSubview:[dynamicLabels objectAtIndex:0]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:1]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:2]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:3]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:4]];
    }
    else{
        UILabel* labelSix = [self createDescriptionLabelWithFrame:CGRectMake(170, 30, 180, 20) andText:@"42"];
        [dynamicLabels addObject:labelSix];
        UILabel* labelSeven = [self createDescriptionLabelWithFrame:CGRectMake(170, 60, 180, 20) andText:@"Cirujano"];
        [dynamicLabels addObject:labelSeven];
        UILabel* labelEight = [self createDescriptionLabelWithFrame:CGRectMake(170, 90, 180, 20) andText:@"+(57)(2) 555 55 55"];
        [dynamicLabels addObject:labelEight];
        UILabel* labelNine = [self createDescriptionLabelWithFrame:CGRectMake(170, 120, 180, 20) andText:@"+(57)(318) 345 67 89"];
        [dynamicLabels addObject:labelNine];
        UILabel* labelTen = [self createDescriptionLabelWithFrame:CGRectMake(170, 150, 180, 60) andText:@"Calle 1a #75 120"];
        labelTen.lineBreakMode = YES;
        [dynamicLabels addObject:labelTen];
        //NSLog(@"Labels : %@", dynamicLabels);
        
        [viewOne addSubview:[dynamicLabels objectAtIndex:0]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:1]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:2]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:3]];
        [viewOne addSubview:[dynamicLabels objectAtIndex:4]];
    }
    
    //[labelSix release]; [labelSeven release]; [labelEight release]; [labelNine release]; [labelTen release];
    
    

	[viewOne addSubview:labelOne];
    [viewOne addSubview:labelTwo];
    [viewOne addSubview:labelThree];
    [viewOne addSubview:labelFour];
    [viewOne addSubview:labelFive];
    
    [labelOne release]; [labelTwo release]; [labelThree release]; [labelFour release]; [labelFive release];
	[self.accordion addHeader:headerOne withView:viewOne];
    
    //----------------------------------------
	//the next section in the accordion
	UIButton* headerTwo = [self createAccordionHeaderButtonWithFrame:CGRectMake(0, 350 , 0, 40) andTitle:@"Último Ingreso"];
	
	//second section content
	UIView* viewTwo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewTwo setBackgroundColor:[UIColor whiteColor]];
	//el contenido
	UILabel* labelEleven = [self createTitleLabelWithFrame:CGRectMake(30, 30, 100, 20) andText:@"Fecha"];
	UILabel* labelTwelve = [self createTitleLabelWithFrame:CGRectMake(30, 60, 100, 20) andText:@"Fecha"];
	
	[viewTwo addSubview:labelEleven]; [viewTwo addSubview:labelTwelve];
	[labelEleven release];
    [labelTwelve release];
	
	[self.accordion addHeader:headerTwo withView:viewTwo];
	[self.accordion setNeedsLayout];
	[self.accordion setAllowsMultipleSelection:YES];	
	[self.view addSubview:self.accordion];
    [self.view sendSubviewToBack:self.accordion];
	
}

-(void)gradientBackground
{
    self.gradientView = [[MMTCSGradientView alloc] init];
	self.gradientView.frame = self.view.bounds;
	[self.view addSubview:self.gradientView];
	[self.view sendSubviewToBack:self.gradientView];
}
//------------------------------------------------------------------------------
#pragma mark - Private Methods

- (UIButton *)createAccordionHeaderButtonWithFrame:(CGRect)frame andTitle:(NSString *)title;
{
    UIButton* theButton = [[UIButton alloc] initWithFrame:frame];
	[theButton setTitle:title forState:UIControlStateNormal];
    //color and image styles
	[theButton setTitleColor:[UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1] forState:UIControlStateNormal];
	[theButton setBackgroundImage:[UIImage imageNamed:@"tab_bg.png"] forState:UIControlStateNormal];
    //title label styles
	[theButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
	[theButton.titleLabel setShadowColor:[UIColor blackColor]];
	[theButton.titleLabel setShadowOffset:CGSizeMake(0, 1)];
    
    return theButton;
}


- (UILabel *)createTitleLabelWithFrame:(CGRect)frame andText:(NSString *)text
{
    UILabel *theLabel = [[UILabel alloc] initWithFrame:frame];
    [theLabel setText:text];
    [theLabel setTextColor:[UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1]];
    theLabel.font = [UIFont boldSystemFontOfSize:14];
    
    return theLabel;
}


- (UILabel *)createDescriptionLabelWithFrame:(CGRect)frame andText:(NSString *)text
{
    UILabel *theLabel = [[UILabel alloc] initWithFrame:frame];
    [theLabel setText:text];
    [theLabel setTextColor:[UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:1]];
    theLabel.font = [UIFont boldSystemFontOfSize:13];
    
    return theLabel;
}

-(void)updateInfoDropdown:(NSManagedObject *)mangedObject;
{
    if([mangedObject isKindOfClass:[Physician class]]){
        Physician *physician = (Physician *)mangedObject;
        
        [[dynamicLabels objectAtIndex:0] setText:[physician.age stringValue]];
        [[dynamicLabels objectAtIndex:1] setText:physician.speciality];
        [[dynamicLabels objectAtIndex:2] setText:physician.phone];
        [[dynamicLabels objectAtIndex:3] setText:physician.cellphone];
        [[dynamicLabels objectAtIndex:4] setText:physician.address];
        [self.physicianPhoto setImage:[UIImage imageWithContentsOfFile:physician.photo]];
    }
}


//-------------------------------------------------------------------------------------------------------
#pragma mark - Storyboard methods

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"gotoPatientInfo"]){
        MMTCSPatientInfoViewController *patientInfo = (MMTCSPatientInfoViewController *)segue.destinationViewController;
        
        if([sender isKindOfClass:[MMTCSDetailViewController class]]){
            MMTCSDetailViewController* controller = (MMTCSDetailViewController *)sender;
            patientInfo.currentPatient = [controller.patients objectAtIndex:[controller selectedPatient]];
        }
    }
    if ([segue.identifier isEqualToString:@"gotoPatientHistory"]) {
        MMTCSMedicalHistoryMasterViewController *patientInfo = (MMTCSMedicalHistoryMasterViewController *)segue.destinationViewController;
       
        if([sender isKindOfClass:[MMTCSDetailViewController class]]){
            MMTCSDetailViewController* controller = (MMTCSDetailViewController *)sender;
            patientInfo.currentPatient = [controller.patients objectAtIndex:controller.selectedPatient];
        }
    }
    if ([segue.identifier isEqualToString:@"LoginMaster"]) {
        MMTCSLoginViewController* controller = (MMTCSLoginViewController *)segue.destinationViewController;
        controller.delegate = self;
        
    }
}

//-------------------------------------------------------------------------------------------------------
#pragma mark - LoginViewcontroller delegate methods

-(void)loginViewControllerDidMadeLoginRequest:(MMTCSLoginViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
