//
//  MMTCSCreateAccountViewController.h
//  Mobimed
//
//  Created by evento on 13/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMTCSCommon.h"
#import "MMTCSGradientView.h"
//
// The delegate protocol to dismiss the view
//
@class MMTCSCreateAccountViewController;
@protocol MMTCSCreateAccountViewControllerDelegate <NSObject>

- (void)createAccountShouldDismiss:(MMTCSCreateAccountViewController *)controller;

@end

//--------------------------------------------------------------

@interface MMTCSCreateAccountViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIView *formContainer;

@property (retain, nonatomic) IBOutletCollection(UITextField) NSArray *createAccountTextFields;


@property (retain, nonatomic) id<MMTCSCreateAccountViewControllerDelegate> delegate;

#pragma mark - Methods

- (IBAction)returnToLoginScreen:(id)sender;

- (IBAction)createAccount:(id)sender;

@end
