//
//  MMTCSPatientsTileView.h
//  Mobimed
//
//  Created by evento on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

/////////////////
//The enum with the presentation styles
/////////////////
#pragma mark - Presentation Styles
typedef enum _PatientsTileViewPresentationStyle{
    PatientsTileViewPresentationStylePhoto = 0,
    PatientsTileViewPresentationStyleSimple = 1
} PatientsTileViewPresentationStyle;

//-------------------------------------------------

@interface MMTCSPatientsTileView : UIControl{
    
    CGFloat stylePhotoObjWidth;
    CGFloat stylePhotoObjHeight;
    CGFloat styleSimpleObjWidth;
    CGFloat styleSimpleObjHeight;
    
    UILabel* staticHourLabel;
    UILabel* staticPlaceLabel;
    UILabel* staticDurationLabel;
    UILabel* staticDateLabel;
}

//////
//
//////
#pragma mark - Properties
@property (nonatomic, assign) PatientsTileViewPresentationStyle presentationStyle;

@property (nonatomic, retain) UIImageView *pictureUIImageView;

@property (nonatomic,retain) UILabel *patientNameLabel;

@property (nonatomic,retain) UILabel* hourLabel;

@property (nonatomic,retain) UILabel* placeLabel;

@property (nonatomic,retain) UILabel* durationLabel;

@property (nonatomic,retain) UILabel* dateLabel;




#pragma mark - Public Methods

- (id)initWithPresentationStyle:(PatientsTileViewPresentationStyle)style;

-(void)setPatientImage:(UIImage*)image withInfo:(NSString*)nombre_ dateHour:(NSString*)horaConsulta_ place:(NSString*)lugar_ duration:(NSString*)duracion_ andDate:(NSString*)fechaConsulta_;

-(CGFloat)stylePhotoObjWidth;

-(void)setStylePhotoObjWidth:(CGFloat)width;

-(CGFloat)stylePhotoObjHeight;

-(void)setStylePhotoObjHeight:(CGFloat)height;

-(CGFloat)styleSimpleObjWidth;

-(void)setStyleSimpleObjWidth:(CGFloat)width;

-(CGFloat)styleSimpleObjHeight;

-(void)setStyleSimpleObjHeight:(CGFloat)height;

@end
