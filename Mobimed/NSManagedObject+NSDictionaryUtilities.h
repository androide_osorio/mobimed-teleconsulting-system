//
//  NSManagedObject+NSDictionaryUtilities.h
//  Mobimed
//
//  Created by evento on 22/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (NSDictionaryUtilities)

@property (assign, nonatomic) BOOL traversed;

#pragma mark - methods

- (NSDictionary*) toDictionary;
- (void) populateFromDictionary:(NSDictionary*)dict;
+ (NSManagedObject*) createManagedObjectFromDictionary:(NSDictionary*)dict 
                                             inContext:(NSManagedObjectContext*)context;

@end
