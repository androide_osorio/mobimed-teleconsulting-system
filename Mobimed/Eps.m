//
//  Eps.m
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Eps.h"
#import "Patient.h"


@implementation Eps

@dynamic id;
@dynamic insurance_number;
@dynamic name;
@dynamic regime;
@dynamic services;
@dynamic patient;

@end
