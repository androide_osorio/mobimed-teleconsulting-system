//
//  Patient.h
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, Consultation, Eps, Physician;

@interface Patient : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSDate * birth_date;
@property (nonatomic, retain) NSString * cellphone;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * ethnicity;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * history;
@property (nonatomic, retain) NSString * home_phone;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * marital_status;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSString * religion;
@property (nonatomic, retain) NSString * work_phone;
@property (nonatomic, retain) NSSet *appointments;
@property (nonatomic, retain) NSSet *consultation;
@property (nonatomic, retain) Eps *eps;
@property (nonatomic, retain) Physician *physician;
@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addAppointmentsObject:(Appointment *)value;
- (void)removeAppointmentsObject:(Appointment *)value;
- (void)addAppointments:(NSSet *)values;
- (void)removeAppointments:(NSSet *)values;

- (void)addConsultationObject:(Consultation *)value;
- (void)removeConsultationObject:(Consultation *)value;
- (void)addConsultation:(NSSet *)values;
- (void)removeConsultation:(NSSet *)values;

@end
