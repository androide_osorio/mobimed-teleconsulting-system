//
//  MMTCSMiniClinicalHistoryViewController.h
//  Mobimed
//
//  Created by evento on 13/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMTCSMiniClinicalHistoryViewController;
@protocol MMTCSMiniClinicalHistoryViewControllerDelegate <NSObject>

- (void)dismissMiniClinicalHistory:(MMTCSMiniClinicalHistoryViewController *)controller;

@end

//---------------------------------------

@interface MMTCSMiniClinicalHistoryViewController : UIViewController

@property(retain, nonatomic) Patient *currentPatient;

@property (retain, nonatomic) IBOutlet UIWebView *historyWebView;

@property (retain, nonatomic) id<MMTCSMiniClinicalHistoryViewControllerDelegate> delegate;

- (IBAction)dismiss:(id)sender;

@end
