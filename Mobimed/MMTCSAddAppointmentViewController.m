//
//  MMTCSAddAppointmentViewController.m
//  Mobimed
//
//  Created by evento on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSAddAppointmentViewController.h"

@implementation MMTCSAddAppointmentViewController
@synthesize delegate;
@synthesize titleTextField;
@synthesize durationTextField;
@synthesize placeTextField;
@synthesize dateCell;
@synthesize patientCell;
@synthesize patient;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-splash.png"]];
}


- (void)viewDidUnload
{
 
    [self setTitleTextField:nil];
    [self setDurationTextField:nil];
    [self setPlaceTextField:nil];
    [self setDateCell:nil];
    [self setPatientCell:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-splash.png"]];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255 blue:85.0/255 alpha:1];
    [super viewWillAppear:animated];
}

//------------------------------------------------------------------------
#pragma mark - UITableViewDelegate and UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1){
        if(indexPath.row == 0){
            [self.titleTextField becomeFirstResponder];
        }
        else if(indexPath.row == 1){
            [self.durationTextField becomeFirstResponder];
        }
        else if(indexPath.row == 2){
            [self.titleTextField becomeFirstResponder];
        }
        else if(indexPath.row == 4){
            
        }
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(0,200,300,244)];
    tempView.backgroundColor=[UIColor clearColor];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(35,0,300,44)];
    tempLabel.backgroundColor=[UIColor clearColor]; 
    tempLabel.shadowColor = [UIColor blackColor];
    tempLabel.shadowOffset = CGSizeMake(0,1);
    tempLabel.textColor = [UIColor whiteColor]; //here u can change the text color of header
    tempLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    tempLabel.font = [UIFont boldSystemFontOfSize:18];
    tempLabel.text= (section == 0) ? @"Paciente" : @"Información Adicional";
    
    [tempView addSubview:tempLabel];
    
    [tempLabel release];
    return tempView;
}

//------------------------------------------------------------------------
#pragma mark - IBAction Methods

- (IBAction)cancel:(id)sender {
    [self.delegate MMTCSAddAppointmentViewControllerDidCancel:self];
}

- (IBAction)create:(id)sender {
    MMTCSNetworkManager *manager = [(MMTCSAppDelegate *)[[UIApplication sharedApplication] delegate] networkManager];
    Physician *physician;
    //set the physician from core data
    NSManagedObjectContext *context = [(MMTCSAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Physician" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:description];
    
    NSError *error;
    //get the actual objects from the database in an array
    NSArray* fetchedObjects = [context executeFetchRequest:request error:&error];
    physician = (Physician *)[fetchedObjects objectAtIndex:0];
    if(!error){
        
    }
    
    NSDictionary *newAppointment = [NSDictionary dictionaryWithObjectsAndKeys:
                                    titleTextField.text ,@"data[Appointment][title]",
                                    dateCell.detailTextLabel.text, @"data[Appointment][date]",
                                    durationTextField.text, @"data[Appointment][duration]",
                                    placeTextField.text, @"data[Appointment][place]", 
                                    patient.id, @"data[Appointment][patient_id]", 
                                    physician.id,@"data[Appointment][physician_id]" , nil];

    
    [manager createFormDataRequestWithValues:newAppointment toURL:@"addAppointment"];
    
    [self.delegate MMTCSAddAppointmentViewControllerDidCreate:self];
}


- (void)dealloc {
    [titleTextField release];
    [durationTextField release];
    [placeTextField release];
    [dateCell release];
    [patientCell release];
    [super dealloc];
}

//--------------------------------------------------------------------------
#pragma mark - custom view delegate methods

-(void)viewcontrollerDidSubmitDate:(MMTCSAppointmentDateViewController *)controller
{
    NSDateFormatter* format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"EEE dd MMMM yyyy HH:mm"];
    
    NSString* dateString = [format stringFromDate:controller.currentDate];
    [self.dateCell.detailTextLabel setText:dateString];
    [controller.navigationController popToRootViewControllerAnimated:YES];
}

-(void)patientsListViewControllerDidCancel:(MMTCSPatientsListViewController *)controller
{
    [controller.navigationController popToRootViewControllerAnimated:YES];
}

-(void)patientsListViewControllerDidSubmit:(MMTCSPatientsListViewController *)controller
{
    self.patient = controller.selectedPatient;
    [self.patientCell.textLabel setText:controller.selectedPatient.name];
    [controller.navigationController popToRootViewControllerAnimated:YES];
}

//-------------------------------------------------------------------------
#pragma mark - Storyboard methods


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"pickDate"]){
        MMTCSAppointmentDateViewController* controller = segue.destinationViewController;
        controller.delegate = self;
    }
    if([segue.identifier isEqualToString:@"pickPatient"]){
        MMTCSPatientsListViewController* controller = segue.destinationViewController;
        controller.delegate = self;
    }
}

@end
