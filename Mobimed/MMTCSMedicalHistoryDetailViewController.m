//
//  MMTCSVideoCallViewController.m
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSMedicalHistoryDetailViewController.h"
#import "MMTCSDetailViewController.h"

@interface MMTCSMedicalHistoryDetailViewController ()

@property (strong, nonatomic) UIPopoverController *masterPopoverController;

@end


static NSString* const HISTORY_CONSULTATIONS_URL = @"http://mobimedtest.comuf.com/consultations/consultationsHistoryByPatientId";

@implementation MMTCSMedicalHistoryDetailViewController

@synthesize historyWebView;
@synthesize currentPatient;


@synthesize masterPopoverController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)setDetailItem:(id)newDetailItem
{
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */
 

- (void)viewDidUnload
{
    [self setHistoryWebView:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    
    if(self.currentPatient){
        NSString* destinationUrl = [HISTORY_CONSULTATIONS_URL stringByAppendingPathComponent:[self.currentPatient.id stringValue]];
        
        NSURL *url = [NSURL URLWithString:destinationUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.historyWebView loadRequest:request];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

//----------------------------------------------------------------
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Patient", @"Patient");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

//---------------------------------------------------------------------

- (void)dealloc {
    [historyWebView release];
    [super dealloc];
}

/**
 * Storyboard handling methods
 */
#pragma mark - Storyboard and Segues Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Login"])
	{
        
	}
}

//-------------------------------------------------------------------------------
#pragma mark - custom style methods
-(void)applyLabelStyles:(UILabel *)label
{
    [label setFont:[UIFont systemFontOfSize:17]];
	
	[label setTextColor:[UIColor whiteColor]];
	[label setShadowColor:[UIColor blackColor]];
	[label setShadowOffset:CGSizeMake(0, -1)];
	[label setTextAlignment:UITextAlignmentCenter];
	[label setBackgroundColor:[UIColor clearColor]];
}



- (IBAction)backToDashboard:(id)sender 
{
    //NSArray that has all the detail view controllers
    NSArray* rootViewControllers = self.navigationController.viewControllers;
    //get the detail root view controller
    MMTCSDetailViewController* masterViewController = [rootViewControllers objectAtIndex:0];
    //get the UISplitViewcontroller from the root-DetailViewController and get the first UINavigationController
    //(the one of the MasterViewController)
    id rootnav = [masterViewController.splitViewController.viewControllers objectAtIndex:0];
    
    if([rootnav isKindOfClass:[UINavigationController class]])
    {
        UINavigationController* nav = (UINavigationController*)rootnav;
        [nav popToRootViewControllerAnimated:YES];
    }
    self.splitViewController.delegate = [self.navigationController.viewControllers objectAtIndex:0];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
