//
//  MMTCSVideoCallViewController.h
//  Mobimed
//
//  Created by sebaZ on 9/05/12.
//  Copyright (c) 2012 Androide Osorio, sebaZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>
#import "MMTCSMiniClinicalHistoryViewController.h"


@interface MMTCSVideoCallViewController : UIViewController<UISplitViewControllerDelegate, OTSessionDelegate, OTSubscriberDelegate, OTPublisherDelegate, UIAlertViewDelegate, MMTCSMiniClinicalHistoryViewControllerDelegate>

#pragma mark - Properties
@property (retain, nonatomic) IBOutlet UILabel *statusLabel;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (retain, nonatomic) IBOutlet UIImageView *notConnectedImage;

@property (retain, nonatomic) IBOutlet UIToolbar *tollbar;

@property BOOL videoConferenceConnected;

@property BOOL showInstructions;

@property (retain, nonatomic) Patient *currentPatient;

#pragma mark - Methods
- (void)doConnect;

- (void)doDisconnect;

- (void)doPublish;

- (void)doUnpublish;

- (IBAction)backToDashboard:(id)sender;

- (IBAction)disconnect:(id)sender;

- (IBAction)connect:(id)sender;

- (IBAction)showInstructions:(id)sender;


@end
