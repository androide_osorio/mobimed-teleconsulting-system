//
//  MMTCSNetworkManager.h
//  Mobimed
//
//  Created by evento on 22/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIFormDataRequest.h"

@interface MMTCSNetworkManager : NSObject
{
    ASIHTTPRequest* request;
    NSArray *loginCookies;
    ASIFormDataRequest *loginRequest;
    ASINetworkQueue* imagesNetworkQueue;
    ASINetworkQueue* serverQueue;
    SBJsonParser* jsonParser;
    NSURL* baseUrl;
}

@property (retain, nonatomic) NSManagedObjectContext *context;

@property (retain, nonatomic) NSString *username;

@property (retain, nonatomic) NSString * password;

@property (assign, nonatomic) BOOL userLoggedIn;

#pragma mark - Methods

-(void) loginWithUsername:(NSString *)theUsername andPassword:(NSString *)thePassword;

-(void) requestDataFromServer;

-(void) createFormDataRequestWithValues:(NSDictionary *)dict toURL:(NSString *)param;

@end
