//
//  MMTCSPatientInfoViewController.h
//  Mobimed
//
//  Created by Androide Osorio on 9/05/12.
//  Copyright (c) 2012 AndroideOsorio, sebaZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "MMTCSSpeechRecognizer.h"

//------------------------------------------------------------------
#pragma mark - LabelPresentationStyle enum
typedef enum _MMTCSLabelPresentationStyle{
    MMTCSLabelPresentationStyleStatic = 0,
    MMTCSLabelPresentationStyleDynamic,
    MMTCSLabelPresentationStyleBiometricDataStatic,
    MMTCSLabelPresentationStyleBiometricDataDynamic
} MMTCSLabelPresentationStyle;

//------------------------------------------------------------------
#pragma mark - ButtonPresentationStyle enum
typedef enum _MMTCSButtonPresentationStyle{
    MMTCSButtonPresentationStyleAccordionHeader = 0,
    MMTCSButtonPresentationStyleRecord
} MMTCSButtonPresentationStyle;

@class MMTCSGradientView;
@class AccordionView;
@class MMTCSVideoCallViewController;

@interface MMTCSPatientInfoViewController : UIViewController<UITextFieldDelegate>{
    @private
    MMTCSSpeechRecognizer* recognizer;
}

//------------------------------------------------------------------

#pragma mark - properties

@property (strong, nonatomic) MMTCSVideoCallViewController *detailViewController;

@property (nonatomic,retain) MMTCSGradientView *gradientView;

@property (nonatomic) BOOL mirrored;

@property BOOL keyboardIsShowing;

@property (retain, nonatomic) IBOutlet UIScrollView *superContainerScrollView;

@property (nonatomic,retain) NSString* todaysDateString;

@property (nonatomic, retain) AccordionView* accordion;

@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

@property (retain, nonatomic) IBOutlet UILabel *patientNameLabel;

@property (retain, nonatomic) IBOutlet UIImageView *patientPhoto;

@property (retain, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (retain, nonatomic) Patient *currentPatient;

@property (retain, nonatomic) NSMutableArray *dynamicLabels;

@property (retain, nonatomic) IBOutletCollection(UIButton) NSMutableArray *recordButtons;

@property (retain, nonatomic) UIButton *currentRecordButton;

@property (retain, nonatomic) UITextField *tempetatureLabel;

@property (retain, nonatomic) UITextField *pulseLabel;

@property (retain, nonatomic) UITextField *bloodPressureLabel;

@property (retain, nonatomic) UITextField *respiratoryRateLabel;

@property (retain, nonatomic) UITextField *OxygenSaturationLabel;

@property (retain, nonatomic) UITextView *pronosticTextView;

@property (retain, nonatomic) UITextView *diagnosticTextView;

@property (retain, nonatomic) UITextView *treatmentTextView;

@property (retain, nonatomic) UITextView *commentsTextView;

//------------------------------------------------------------------

#pragma mark - public methods

-(IBAction)startRecord:(id)sender;


@end
