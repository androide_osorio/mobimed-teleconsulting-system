//
//  NSString+isEmpty.m
//  Mobimed
//
//  Created by evento on 22/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+isEmpty.h"

@implementation NSString (isEmpty)

-(BOOL)isEmpty
{
    return [self isEqualToString:@""];
}

-(BOOL)isNumeric
{
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i=0; i < [self length]; i++) {
        unichar c = [self characterAtIndex:i]; 
        if(![myCharSet characterIsMember:c]){
            return NO;
        }
    }
    return YES;
}

@end
