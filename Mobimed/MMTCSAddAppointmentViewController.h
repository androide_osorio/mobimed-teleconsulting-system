//
//  MMTCSAddAppointmentViewController.h
//  Mobimed
//
//  Created by evento on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMTCSAppointmentDateViewController.h"
#import "MMTCSPatientsListViewController.h"

@class MMTCSAddAppointmentViewController;
@protocol MMTCSAddAppointmentViewControllerDelegate <NSObject>

-(void)MMTCSAddAppointmentViewControllerDidCreate:(MMTCSAddAppointmentViewController *)controller;

-(void)MMTCSAddAppointmentViewControllerDidCancel:(MMTCSAddAppointmentViewController *)controller;

@end

@interface MMTCSAddAppointmentViewController : UITableViewController<MMTCSAppointmentDateViewControllerDelegate, MMTCSPatientsListViewControllerDelegate>

#pragma mark - Properties
@property (retain, nonatomic) id<MMTCSAddAppointmentViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutlet UITextField *titleTextField;

@property (retain, nonatomic) IBOutlet UITextField *durationTextField;

@property (retain, nonatomic) IBOutlet UITextField *placeTextField;

@property (retain, nonatomic) IBOutlet UITableViewCell *dateCell;

@property (retain, nonatomic) IBOutlet UITableViewCell *patientCell;

@property (retain, nonatomic) Patient *patient;

#pragma mark - Methods
- (IBAction)cancel:(id)sender;

- (IBAction)create:(id)sender;

@end
