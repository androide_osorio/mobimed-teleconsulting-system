//
//  Patient.m
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Patient.h"
#import "Appointment.h"
#import "Consultation.h"
#import "Eps.h"
#import "Physician.h"


@implementation Patient

@dynamic address;
@dynamic age;
@dynamic birth_date;
@dynamic cellphone;
@dynamic city;
@dynamic email;
@dynamic ethnicity;
@dynamic gender;
@dynamic history;
@dynamic home_phone;
@dynamic id;
@dynamic marital_status;
@dynamic name;
@dynamic photo;
@dynamic religion;
@dynamic work_phone;
@dynamic appointments;
@dynamic consultation;
@dynamic eps;
@dynamic physician;

@end
