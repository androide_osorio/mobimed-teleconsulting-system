//
//  Consultation.m
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Consultation.h"
#import "Patient.h"


@implementation Consultation

@dynamic blood_pressure;
@dynamic BMI;
@dynamic comments;
@dynamic current_illness;
@dynamic date;
@dynamic diagnosis;
@dynamic general_checklist;
@dynamic height;
@dynamic id;
@dynamic pronostic;
@dynamic pulse;
@dynamic regional_checklist;
@dynamic respiratory_rate;
@dynamic temperature;
@dynamic treatment;
@dynamic weight;
@dynamic patient;

@end
