//
//  MMTCSAddPatientViewController.m
//  Mobimed
//
//  Created by evento on 13/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSAddPatientViewController.h"

@implementation MMTCSAddPatientViewController
@synthesize delegate;
@synthesize nameTextField;
@synthesize ageTextField;
@synthesize genderTextField;
@synthesize phoneTextField;
@synthesize cellphoneTextField;
@synthesize workphoneTextField;
@synthesize emailTextField;
@synthesize maritalStatusTextfield;
@synthesize ethnicityTextField;
@synthesize relogionTextField;
@synthesize clinicalHistoryTextField;
@synthesize EPSTextField;
@synthesize regimenTextField;
@synthesize insuranceNumberTextField;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setNameTextField:nil];
    [self setAgeTextField:nil];
    [self setGenderTextField:nil];
    [self setPhoneTextField:nil];
    [self setCellphoneTextField:nil];
    [self setWorkphoneTextField:nil];
    [self setEmailTextField:nil];
    [self setMaritalStatusTextfield:nil];
    [self setEthnicityTextField:nil];
    [self setRelogionTextField:nil];
    [self setClinicalHistoryTextField:nil];
    [self setEPSTextField:nil];
    [self setRegimenTextField:nil];
    [self setInsuranceNumberTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        
    }
}

- (void)dealloc {
    [nameTextField release];
    [ageTextField release];
    [genderTextField release];
    [phoneTextField release];
    [cellphoneTextField release];
    [workphoneTextField release];
    [emailTextField release];
    [maritalStatusTextfield release];
    [ethnicityTextField release];
    [relogionTextField release];
    [clinicalHistoryTextField release];
    [EPSTextField release];
    [regimenTextField release];
    [insuranceNumberTextField release];

    [super dealloc];
}

#pragma mark - IBAction Methods

- (IBAction)cancel:(id)sender 
{
    [delegate addPatientDidCancel:self];
}

- (IBAction)submitNewPatient:(id)sender 
{
    [delegate addPatientDidSubmit:self];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [delegate addPatientDidCancel:self];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage * image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    // You have the image. You can use this to present the image in the next view like you require in `#3`.
    
    [self dismissModalViewControllerAnimated:YES];
}

@end
