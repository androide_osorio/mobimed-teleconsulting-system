//
//  Eps.h
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface Eps : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * insurance_number;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * regime;
@property (nonatomic, retain) NSString * services;
@property (nonatomic, retain) Patient *patient;

@end
