//
//  MMTCSVideoCallViewController.m
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSVideoCallViewController.h"
#import "MMTCSDetailViewController.h"

#pragma mark - Private Category declaration
@interface MMTCSVideoCallViewController ()

@property (strong, nonatomic) UIPopoverController *masterPopoverController;

- (void)setStatusLabel;

- (void)updateSubscriber;

-(void)applyLabelStyles:(UILabel *)label;

@end

//
//
//
#pragma mark - Static utility variables

static int topOffset        = 90;
static double widgetHeight  = 240*1.4;
static double widgetWidth   = 320*1.4;
static double otherStreamsX = 0;
static double otherStreamsY = 0;

static NSString* const kApiKey    = @"1127";
static NSString* const kToken     = @"devtoken";
static NSString* const kSessionId = @"2_MX4wfn4yMDEyLTAyLTA0IDAyOjIxOjI0LjE0ODMwNCswMDowMH4wLjQ2MDExMzM1NDczOH4";
static bool subscribeToSelf       = NO; // Change to NO if you want to subscribe streams other than your own

//---------------------------------------------------------------------------------

@implementation MMTCSVideoCallViewController{
    OTSession* _session;
    OTPublisher* _publisher;
    OTSubscriber* _subscriber;
    UIImageView *instructions;
}
@synthesize showInstructions = _showInstructions;

#pragma mark - properties

@synthesize statusLabel = _statusLabel;
@synthesize activityIndicator = _activityIndicator;
@synthesize notConnectedImage = _notConnectedImage;
@synthesize tollbar = _tollbar;
@synthesize masterPopoverController;
@synthesize videoConferenceConnected;
@synthesize currentPatient;


//---------------------------------------------------------------------------------
#pragma mark - initialization Methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)setDetailItem:(id)newDetailItem
{
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)dealloc {
    [_statusLabel release];
    [_activityIndicator release];
    [_notConnectedImage release];
    [_session release];
    [_subscriber release];
    [_publisher release];
    [_tollbar release];
    [super dealloc];
}

//----------------------------------------------------------------
#pragma mark - View lifecycle

- (void)viewDidUnload
{
    [self setActivityIndicator:nil];
    [self setStatusLabel:nil];
    [self setNotConnectedImage:nil];
    _subscriber = nil;
    _publisher  = nil;
    _session    = nil;
    [self setTollbar:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    UIColor* backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-splash.png"]];
	[self.view setBackgroundColor:backgroundColor];
    widgetWidth  = self.view.frame.size.width-30;
    
    //widgetHeight = self.view.frame.size.height;
    self.navigationItem.hidesBackButton = YES;
    self.notConnectedImage.hidden = YES;
    
    [self.activityIndicator startAnimating];
    self.statusLabel.text = @"Conectando...";
    [self applyLabelStyles:self.statusLabel];

    //self.tollbar.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255 blue:85.0/255 alpha:1];
    [self doConnect];
    
    instructions = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VideoConferenciaLandscapeInstructions.png"]];
    instructions.hidden = YES;
    
    [self.view addSubview:instructions];
    self.showInstructions = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [instructions setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [instructions setImage:[UIImage imageNamed:@"VideoConferenciaLandscapeInstructions.png"]];
    }
    else if (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){   
        [instructions setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [instructions setImage:[UIImage imageNamed:@"VideoConferenciaPortraitInstructions.png"]];
    }
	return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

//----------------------------------------------------------------
#pragma mark - SplitViewControllerDelegate Methods

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Patient", @"Patient");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

//---------------------------------------------------------------------
/**
 * Storyboard handling methods
 */
#pragma mark - Storyboard and Segues Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"miniHistory"])
	{
        UINavigationController* navigationController = segue.destinationViewController;
        MMTCSMiniClinicalHistoryViewController* addView = [[navigationController viewControllers] objectAtIndex:0];
        [addView setCurrentPatient:self.currentPatient];
        addView.delegate = self;
	}
}

//-------------------------------------------------------------------------------
#pragma mark - custom style methods
-(void)applyLabelStyles:(UILabel *)label
{
    [label setFont:[UIFont systemFontOfSize:17]];
	
	[label setTextColor:[UIColor whiteColor]];
	[label setShadowColor:[UIColor blackColor]];
	[label setShadowOffset:CGSizeMake(0, -1)];
	[label setTextAlignment:UITextAlignmentCenter];
	[label setBackgroundColor:[UIColor clearColor]];
}
//------------------------------------------------------------------------------
#pragma mark - UIAlertViewDelegate Methods

-(void)alertViewCancel:(UIAlertView *)alertView
{
    //cosa para volver al dashboard
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0){
        [self doUnpublish];
        [self doDisconnect];
        //NSArray that has all the detail view controllers
        NSArray* rootViewControllers = self.navigationController.viewControllers;
        //get the detail root view controller
        MMTCSDetailViewController* masterViewController = [rootViewControllers objectAtIndex:0];
        //get the UISplitViewcontroller from the root-DetailViewController and get the first UINavigationController
        //(the one of the MasterViewController)
        id rootnav = [masterViewController.splitViewController.viewControllers objectAtIndex:0];
        //return to the initial master and detail view controllers
        if([rootnav isKindOfClass:[UINavigationController class]])
        {
            UINavigationController* nav = (UINavigationController*)rootnav;
            [nav popToRootViewControllerAnimated:YES];
        }
        //change the delegates as they must be
        self.splitViewController.delegate = [self.navigationController.viewControllers objectAtIndex:0];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
    //if the user prompted to retry, then retry
    if (buttonIndex == 1) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        [self doConnect];
    }
}

//-----------------------------------------------------------------------
/**
 * Opentok Library Methods
 */
#pragma mark - Opentok Library Methods
- (void)doConnect
{
    //create the session object with the specified session id
    _session = [[OTSession alloc] initWithSessionId:kSessionId
                                           delegate:self];
    //check the connectionCount (hoy many users are connected
    [_session addObserver:self
               forKeyPath:@"connectionCount"
                  options:NSKeyValueObservingOptionNew
                  context:nil];
    //connect with the API Key and token
    [_session connectWithApiKey:kApiKey token:kToken];
}

//disconnect from the session
- (void)doDisconnect
{
    [_session disconnect];
}

//publish the session (the stream) if the connection is successful
- (void)doPublish
{
    //create the publisher (the object that actually makes the video
    _publisher = [[OTPublisher alloc] initWithDelegate:self name:UIDevice.currentDevice.name];
    //publish both audio and video
    _publisher.publishAudio = YES;
    _publisher.publishVideo = YES;
    //do publish the session
    [_session publish:_publisher];
    //draw the session on screen and set its dimensions
    [self.view addSubview:_publisher.view];
    [_publisher.view setFrame:CGRectMake(0, 0, widgetWidth, widgetHeight)];
    [self.view sendSubviewToBack:_publisher.view];
}

//unpublish when the user stops the videocall
- (void)doUnpublish
{
    [_session unpublish:_publisher]; 
}

- (void)updateSubscriber
{
    for (NSString* streamId in _session.streams) {
        OTStream* stream = [_session.streams valueForKey:streamId];
        if (stream.connection.connectionId != _session.connection.connectionId) {
            _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
            break;
        }
    }
}

//-------------------------------------------------------------------------

#pragma mark - OTSessionDelegate methods

- (void)sessionDidConnect:(OTSession*)session
{
    self.statusLabel.text = @"conectado exitosamente";
    NSLog(@"sessionDidConnect: %@", session.sessionId);
    NSLog(@"- connectionId: %@", session.connection.connectionId);
    NSLog(@"- creationTime: %@", session.connection.creationTime);
    [self doPublish];
    self.videoConferenceConnected = YES;
}

- (void)sessionDidDisconnect:(OTSession*)session 
{
    NSLog(@"sessionDidDisconnect: %@", session.sessionId);
    self.statusLabel.text = @"La conexión se ha cerrado.";
    self.videoConferenceConnected = NO;
}

- (void)session:(OTSession*)session didFailWithError:(OTError*)error
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error en la Conexión" 
                                                    message:@"La conexión no pudo ser establecida. Por favor, intente más Tarde." 
                                                   delegate:self 
                                          cancelButtonTitle:@"Escritorio" 
                                          otherButtonTitles:@"Reintentar", nil];
    
    [alert show];
    [alert release];
    self.statusLabel.text = @"Conexión imposible.";
    self.notConnectedImage.hidden = NO;
    self.activityIndicator.hidden = YES;
    NSLog(@"session: didFailWithError:");
    NSLog(@"- error code: %d", error.code);
    NSLog(@"- description: %@", error.localizedDescription);

}

- (void)session:(OTSession*)mySession didReceiveStream:(OTStream*)stream
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;

    NSLog(@"session: didReceiveStream:");
    NSLog(@"- connection.connectionId: %@", stream.connection.connectionId);
    NSLog(@"- connection.creationTime: %@", stream.connection.creationTime);
    NSLog(@"- session.sessionId: %@", stream.session.sessionId);
    NSLog(@"- streamId: %@", stream.streamId);
    NSLog(@"- type %@", stream.type);
    NSLog(@"- creationTime %@", stream.creationTime);
    NSLog(@"- name %@", stream.name);
    NSLog(@"- hasAudio %@", (stream.hasAudio ? @"YES" : @"NO"));
    NSLog(@"- hasVideo %@", (stream.hasVideo ? @"YES" : @"NO"));
    if ( (subscribeToSelf && [stream.connection.connectionId isEqualToString: _session.connection.connectionId])
        ||
        (!subscribeToSelf && ![stream.connection.connectionId isEqualToString: _session.connection.connectionId])
        ) {
        if (!_subscriber) {
            _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
            _subscriber.subscribeToAudio = YES;
            _subscriber.subscribeToVideo = YES;
        }
        NSLog(@"subscriber.session.sessionId: %@", _subscriber.session.sessionId);
        NSLog(@"- stream.streamId: %@", _subscriber.stream.streamId);
        NSLog(@"- subscribeToAudio %@", (_subscriber.subscribeToAudio ? @"YES" : @"NO"));
        NSLog(@"- subscribeToVideo %@", (_subscriber.subscribeToVideo ? @"YES" : @"NO"));
    }
}

- (void)session:(OTSession*)session didDropStream:(OTStream*)stream
{
    NSLog(@"session didDropStream (%@)", stream.streamId);
    if (!subscribeToSelf
        && _subscriber
        && [_subscriber.stream.streamId isEqualToString: stream.streamId]) {
        _subscriber = nil;
        [self updateSubscriber];
    }
}

//----------------------------------------------------------------------------------------
#pragma mark - OTPublisherDelegate methods

- (void)publisher:(OTPublisher*)publisher didFailWithError:(OTError*) error
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error en la transmisión" 
                                                    message:@"La apliacación no pudo crear la salida de audio y Video. Intentelo más tarde" 
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    
    [alert show];
    self.statusLabel.text = @"Imposible de conectar audio y video.";
    self.notConnectedImage.hidden = NO;
    self.activityIndicator.hidden = YES;
    NSLog(@"publisher: %@ didFailWithError:", publisher);
    NSLog(@"- error code: %d", error.code);
    NSLog(@"- description: %@", error.localizedDescription);
}

- (void)publisherDidStartStreaming:(OTPublisher *)publisher
{
    NSLog(@"publisherDidStartStreaming: %@", publisher);
    NSLog(@"- publisher.session: %@", publisher.session.sessionId);
    NSLog(@"- publisher.name: %@", publisher.name);
}

-(void)publisherDidStopStreaming:(OTPublisher*)publisher
{
    NSLog(@"publisherDidStopStreaming:%@", publisher);
}

//----------------------------------------------------------------------------------------

#pragma mark - OTSubscriberDelegate methods

- (void)subscriberDidConnectToStream:(OTSubscriber*)subscriber
{
    NSLog(@"subscriberDidConnectToStream (%@)", subscriber.stream.connection.connectionId);
    [subscriber.view setFrame:CGRectMake(otherStreamsX, otherStreamsY + widgetHeight, widgetWidth,widgetHeight)];
    //otherStreamsX += widgetWidth/2;
    [self.view addSubview:subscriber.view];
    [self.view sendSubviewToBack:subscriber.view];
}

- (void)subscriberVideoDataReceived:(OTSubscriber*)subscriber {
    NSLog(@"subscriberVideoDataReceived (%@)", subscriber.stream.streamId);
}

- (void)subscriber:(OTSubscriber *)subscriber didFailWithError:(OTError *)error
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error en la Subscripcion" 
                                                    message:@"La apliacación no se pudo suscribir a la video-llamada. Intentelo más tarde" 
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    
    [alert show];
    
    NSLog(@"subscriber: %@ didFailWithError: ", subscriber.stream.streamId);
    NSLog(@"- code: %d", error.code);
    NSLog(@"- description: %@", error.localizedDescription);
}

//--------------------------------------------------------------------------------
#pragma mark - KVO(Key-Value observing) methods
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"connectionCount"]) {
        [self setStatusLabel];
    }
}

- (void)setStatusLabel
{
    if (_session && _session.connectionCount > 0) {
        //self.statusLabel.text = [NSString stringWithFormat:@"Connections: %d Streams: %d", _session.connectionCount, _session.streams.count];
        self.statusLabel.hidden = YES;
    } else {
        self.statusLabel.hidden = NO;
        self.statusLabel.text = @"No Conectado";
    }
}

//----------------------------------------------------------------------------------------

#pragma mark - Action Methods
- (IBAction)backToDashboard:(id)sender
{
    [self doUnpublish];
    [self doDisconnect];
    //NSArray that has all the detail view controllers
    NSArray* rootViewControllers = self.navigationController.viewControllers;
    //get the detail root view controller
    MMTCSDetailViewController* masterViewController = [rootViewControllers objectAtIndex:0];
    //get the UISplitViewcontroller from the root-DetailViewController and get the first UINavigationController
    //(the one of the MasterViewController)
    id rootnav = [masterViewController.splitViewController.viewControllers objectAtIndex:0];
    
    if([rootnav isKindOfClass:[UINavigationController class]])
    {
        UINavigationController* nav = (UINavigationController*)rootnav;
        [nav popToRootViewControllerAnimated:YES];
    }
    self.splitViewController.delegate = [self.navigationController.viewControllers objectAtIndex:0];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)disconnect:(id)sender {
    [self doUnpublish];
    [self doDisconnect];
}

- (IBAction)connect:(id)sender {
    [self doConnect];
}

-(void)dismissMiniClinicalHistory:(MMTCSMiniClinicalHistoryViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showInstructions:(id)sender {
    self.showInstructions = !self.showInstructions;
    instructions.hidden = self.showInstructions;
    
}
@end
