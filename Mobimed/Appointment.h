//
//  Appointment.h
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface Appointment : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Patient *patient;

@end
