//
//  MMTCSAppDelegate.h
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MMTCSAppDelegate : UIResponder <UIApplicationDelegate>

//--------------------------------------------
/**
 * App delegate's properties
 */
#pragma mark - Properties

@property (retain, nonatomic) MMTCSNetworkManager *networkManager;

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


//--------------------------------------------
/**
 * The App delegate Methods
 */
#pragma mark - Methods

- (void)saveContext;

- (NSURL *)applicationDocumentsDirectory;

@end
