//
//  MMTCSMasterViewController.h
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "MMTCSLoginViewController.h"

@class MMTCSDetailViewController;
@class MMTCSGradientView;
@class AccordionView;

@interface MMTCSMasterViewController : UIViewController<MMTCSLoginViewControllerDelegate>

@property (strong, nonatomic) MMTCSDetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) Physician *loggedPhysician;

//-----------------------------------------------------------------

@property (nonatomic,retain) MMTCSGradientView *gradientView;

@property (nonatomic) BOOL mirrored;

@property (nonatomic,retain) NSString* todaysDateString;

@property (nonatomic, retain) AccordionView* accordion;

@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

@property (retain, nonatomic) IBOutlet UIImageView *physicianPhoto;

@property (retain, nonatomic) IBOutlet UILabel *physicianNameLabel;

@property (retain, nonatomic) NSMutableArray *dynamicLabels;

//-----------------------------------------------------------------------------------
#pragma mark -
#pragma mark Methods

-(void) todaysDate;

-(void) dropdownMenu;

-(void)gradientBackground;



@end
