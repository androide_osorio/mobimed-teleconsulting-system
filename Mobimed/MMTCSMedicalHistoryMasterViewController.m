//
//  MMTCSMedicalHistoryMasterViewController.m
//  Mobimed
//
//  Created by evento on 18/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSMedicalHistoryMasterViewController.h"
#import "MMTCSPatientsTileView.h"
#import "AccordionView.h"
#import "MMTCSGradientView.h"

@interface MMTCSMedicalHistoryMasterViewController ()

-(void)dropdownMenu;

-(void)gradientBackground;

- (UIButton *)createButtonWithFrame:(CGRect)frame withPresentationStyle:(MMTCSButtonPresentationStyle)style andTitle:(NSString *)title;

- (UILabel *)createLabelWithFrame:(CGRect)frame withPresentationStyle:(MMTCSLabelPresentationStyle)style andText:(NSString *)text;

@end

static UIColor* grayColor;
static UIColor* grayishBlueColor;
static UIColor* lightGrayColor;

//---------------------------------------------------------------------------
//
//
// The actual Class Implementation
//
//
@implementation MMTCSMedicalHistoryMasterViewController
@synthesize patientImage;
@synthesize patientNameLabel;
@synthesize gradientView;
@synthesize mirrored;
@synthesize accordion;
@synthesize detailViewController;
@synthesize currentPatient;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)awakeFromNib
{
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{ 
    [super viewDidLoad];
    NSLog(@"%@", self.currentPatient);
    if(self.currentPatient){
        self.patientNameLabel.text = self.currentPatient.name;
        [self.patientImage setImage:[UIImage imageWithContentsOfFile:self.currentPatient.photo]];
        
    }
    self.navigationItem.hidesBackButton = YES;
    
    //initialize colors
    grayColor = [UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:1];
	grayishBlueColor = [UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1];
    lightGrayColor = [UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:0.1];
    
    [self gradientBackground];
    [self dropdownMenu];
    patientImage.layer.cornerRadius = 13.0;
    patientImage.layer.masksToBounds = YES;
    
    // Do any additional setup after loading the view, typically from a nib.
    self.detailViewController = (MMTCSMedicalHistoryDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    UILabel* staticHourLabel = [[self createLabelWithFrame:CGRectMake(30, 180, 150, 20) 
                                     withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                   andText:@"Última consulta"] autorelease];
    
    
    UILabel* staticDurationLabel = [[self createLabelWithFrame:CGRectMake(30, 206, 130, 20)
                                         withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                       andText:@"Duración"] autorelease];
    
    UILabel*  dynamicHourLabel= [[self createLabelWithFrame:CGRectMake(136, 180, 150, 20)
                                      withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                    andText:@"3 de Abril, 2012" ] autorelease];
    [dynamicHourLabel setTextAlignment:UITextAlignmentRight];
    
    UILabel* dynamicDurationLabel = [[self createLabelWithFrame:CGRectMake(136, 206, 150, 20)
                                          withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                        andText:@"1 Hora" ] autorelease];
    [dynamicDurationLabel setTextAlignment:UITextAlignmentRight];
    
    //---------------------------------
    
    
    [self.view addSubview:staticHourLabel];
    [self.view addSubview:staticDurationLabel];
    [self.view addSubview:dynamicHourLabel];
    [self.view addSubview:dynamicDurationLabel];
    
    
    [self gradientBackground];
}


- (void)viewDidUnload
{
    [self setPatientImage:nil];
    [self setPatientNameLabel:nil];
    [self setAccordion:nil];
    [self setGradientView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)dealloc {
    [patientImage release];
    [patientNameLabel release];
    [accordion release];
    [gradientView release];
    [super dealloc];
}

//-----------------------------------------------------------------
#pragma mark - Helper Methods

-(void)dropdownMenu
{
    self.accordion = [[AccordionView alloc] initWithFrame:CGRectMake(0, 233, 320, 500)];
	
	/** HEADER DATOS BIOMETRICOS **/
    UIButton* header1 = [[self createButtonWithFrame:CGRectMake(0, 0, 0, 43)
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader
                                            andTitle:@"Información Personal"]autorelease];
	
	UIView* viewOne = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewOne setBackgroundColor:[UIColor whiteColor]];
	
	/** labels estáticos datos biométricos**/
    UILabel* labelTemperatura       = [[self createLabelWithFrame:CGRectMake(30, 30, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleStatic
                                                          andText:@"Edad"] autorelease];
    
    UILabel* labelRitmoCardiaco     = [[self createLabelWithFrame:CGRectMake(30, 60, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                          andText:@"F. de nacimiento"] autorelease];
    UILabel* labelPresionArterial   = [[self createLabelWithFrame:CGRectMake(30, 90, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleStatic
                                                          andText:@"Género"] autorelease];     
    UILabel* labelRitmoRespiratorio = [[self createLabelWithFrame:CGRectMake(30, 120, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                          andText:@"Estado Civil"] autorelease];
    UILabel* labelReligion          = [[self createLabelWithFrame:CGRectMake(30, 150, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                          andText:@"Religión"] autorelease]; 
	
	/** labels dinámicos datos biometricos**/
    UILabel* labelDinamicoTemperatura = [self createLabelWithFrame:CGRectMake(170, 30, 150, 20)
                                             withPresentationStyle:MMTCSLabelPresentationStyleDynamic
                                                           andText:[self.currentPatient.age stringValue]];
	
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"dd - MMMM,YYYY"];
    NSString *date = [formatter stringFromDate:self.currentPatient.birth_date];
    UILabel* labelDinamicoRitmoCardiaco = [self createLabelWithFrame:CGRectMake(170, 60, 150, 20)
                                               withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                             andText:date];
	//UILabel* labelDinamicoPresionArterial = [self createBioDataLabelWithFrame:CGRectMake(190, 75, 80, 40) andText:[NSString stringWithFormat:@"%@mmHg", 130]];
    UILabel* labelDinamicoPresionArterial = [self createLabelWithFrame:CGRectMake(170, 90, 150, 20)
                                                 withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                               andText:self.currentPatient.gender];
	//UILabel* labelDinamicoRitmoRespiratorio = [self createBioDataLabelWithFrame:CGRectMake(190, 135, 80, 40) andText:[NSString stringWithFormat:@"%@Fr", 26]];
    UILabel* labelDinamicoRitmoRespiratorio = [self createLabelWithFrame:CGRectMake(170, 120, 150, 20)
                                                   withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                                 andText:self.currentPatient.marital_status];
    UILabel* labelDinamicoReligion          = [self createLabelWithFrame:CGRectMake(170, 150, 150, 20)
                                                   withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                                 andText:self.currentPatient.religion];
    
    [viewOne addSubview:labelDinamicoTemperatura];
    [viewOne addSubview:labelTemperatura];
	[viewOne addSubview:labelRitmoCardiaco];
    [viewOne addSubview:labelPresionArterial];
    [viewOne addSubview:labelRitmoRespiratorio];
    [viewOne addSubview:labelDinamicoRitmoRespiratorio];
	[viewOne addSubview:labelDinamicoRitmoCardiaco];
    [viewOne addSubview:labelDinamicoPresionArterial];
    [viewOne addSubview:labelDinamicoReligion];
    [viewOne addSubview:labelReligion];

    
	[self.accordion addHeader:header1 withView:viewOne];
    
	
	/** HEADER INFORMACIÓN PERSONAL  **/
	
	//UIButton* header2 = [self createAccordionHeaderButtonWithFrame:CGRectMake(0, 0, 0, 40) andTitle:@"Información Personal"];
    UIButton* header2 = [[self createButtonWithFrame:CGRectMake(0, 0, 0, 43) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Información de Contacto"] autorelease];
	
	UIView* viewTwo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewTwo setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos información personal **/
    UILabel* labelNombre    = [[self createLabelWithFrame:CGRectMake(30, 30, 100, 20) 
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Teléfono"] autorelease];
    UILabel* labelEdad      = [[self createLabelWithFrame:CGRectMake(30, 60, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Celular"] autorelease];
    UILabel* labelTelefono  = [[self createLabelWithFrame:CGRectMake(30, 90, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Dirección"] autorelease];
    UILabel* labelDireccion = [[self createLabelWithFrame:CGRectMake(30, 120, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Ciudad"] autorelease];
    UILabel* labelEmail     = [[self createLabelWithFrame:CGRectMake(30, 150, 100, 20)
                                    withPresentationStyle:MMTCSLabelPresentationStyleStatic 
                                                  andText:@"Email"] autorelease];
    
	
	/** labels dinámicos información personal **/
    UILabel* labelDinamicoNombre = [[self createLabelWithFrame:CGRectMake(170, 30, 150, 20)
                                         withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                       andText:self.currentPatient.home_phone] autorelease];
    UILabel* labelDinamicoEdad = [[self createLabelWithFrame:CGRectMake(170, 60, 100, 20)
                                       withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                     andText:self.currentPatient.cellphone] autorelease];
    UILabel* labelDinamicoTelefono = [[self createLabelWithFrame:CGRectMake(170, 90, 100, 20)
                                           withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                         andText:self.currentPatient.address] autorelease];
    
    UILabel* labelDinamicoDireccion = [[self createLabelWithFrame:CGRectMake(170, 120, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                          andText:self.currentPatient.city] autorelease];
    UILabel* labelDinamicoEmail     = [[self createLabelWithFrame:CGRectMake(170, 150, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleDynamic 
                                                          andText:self.currentPatient.email] autorelease];
	
	[viewTwo addSubview:labelNombre];
    [viewTwo addSubview:labelEdad];
    [viewTwo addSubview:labelTelefono]; 
    [viewTwo addSubview:labelDireccion];
    [viewTwo addSubview:labelDinamicoTelefono];
    [viewTwo addSubview:labelDinamicoEdad];
	[viewTwo addSubview:labelDinamicoNombre]; 
    [viewTwo addSubview:labelDinamicoDireccion];
    [viewTwo addSubview:labelEmail];
    [viewTwo addSubview:labelDinamicoEmail];
	[self.accordion addHeader:header2 withView:viewTwo];
    
    [viewTwo release];
	
    
	/** HEADER INFO ULTIMA CONSULTA **/
    UIButton* header3 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Información de la EPS"] autorelease];
    
	UIView* viewThree = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewThree setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    UILabel* labelFecha = [[self createLabelWithFrame:CGRectMake(30, 30, 100, 20)
                                withPresentationStyle:MMTCSLabelPresentationStyleStatic
                                              andText:@"EPS"] autorelease]; 
    UILabel* labelHora = [[self createLabelWithFrame:CGRectMake(30, 60, 100, 20)
                               withPresentationStyle:MMTCSLabelPresentationStyleStatic
                                             andText:@"Regimen"] autorelease]; 
    UILabel* labelDuracion = [[self createLabelWithFrame:CGRectMake(30, 90, 100, 20)
                                   withPresentationStyle:MMTCSLabelPresentationStyleStatic
                                                 andText:@"Número"] autorelease]; 
    UILabel* labelServicios = [[self createLabelWithFrame:CGRectMake(30, 120, 100, 20)
                                   withPresentationStyle:MMTCSLabelPresentationStyleStatic
                                                 andText:@"Servicios"] autorelease]; 
	
	/** labels dinámicos header info ultima consulta **/
    UILabel* labelDinamicoFecha = [[self createLabelWithFrame:CGRectMake(130, 30, 200, 20)
                                        withPresentationStyle:MMTCSLabelPresentationStyleDynamic
                                                      andText:@"EPS Sura"] autorelease]; 
    
    UILabel* labelDinamicoHora =  [[self createLabelWithFrame:CGRectMake(130, 60, 100, 20)
                                        withPresentationStyle:MMTCSLabelPresentationStyleDynamic
                                                      andText:@"Contributivo"] autorelease]; 
    
    UILabel* labelDinamicoDuracion =  [[self createLabelWithFrame:CGRectMake(130, 90, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleDynamic
                                                          andText:@"1234456"] autorelease]; 
    
    UILabel* labelDinamicoServicios =  [[self createLabelWithFrame:CGRectMake(130, 120, 100, 20)
                                            withPresentationStyle:MMTCSLabelPresentationStyleDynamic
                                                          andText:@"Todos"] autorelease]; 
	
	[viewThree addSubview:labelFecha];
    [viewThree addSubview:labelHora];
    [viewThree addSubview:labelDuracion];
    [viewThree addSubview:labelDinamicoFecha];
    [viewThree addSubview:labelDinamicoHora];
    [viewThree addSubview:labelDinamicoDuracion];
    [viewThree addSubview:labelServicios];
    [viewThree addSubview:labelDinamicoServicios];
    
	[self.accordion addHeader:header3 withView:viewThree];
	
    [viewThree release];
	//---------------------
    
    /** HEADER INFO ULTIMA CONSULTA **/
    UIButton* header4 = [[self createButtonWithFrame:CGRectMake(0, 0 , 0, 40) 
                               withPresentationStyle:MMTCSButtonPresentationStyleAccordionHeader 
                                            andTitle:@"Historia Clínica"] autorelease];
    
	UIView* viewFour = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
	[viewFour setBackgroundColor:[UIColor whiteColor]];
    
	/** labels estáticos info ultima consulta **/
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(30, 30, 240, 190)];
    [textView setText:self.currentPatient.history];
    [textView setFont:[UIFont boldSystemFontOfSize:13]];
    [textView setTextColor:grayColor];

	[viewFour addSubview:textView];

    
	[self.accordion addHeader:header4 withView:viewFour];
	
    [viewFour release];
	
	[self.accordion setNeedsLayout];
	[self.accordion setAllowsMultipleSelection:YES];	
	[self.view addSubview:self.accordion];
}

-(void)gradientBackground
{
    self.gradientView = [[MMTCSGradientView alloc] init];
	[self.view addSubview:self.gradientView];
	[self.view sendSubviewToBack:self.gradientView];
}

//----------------------------------------------------------------
#pragma mark - UIHelper Methods

-(UIButton *)createButtonWithFrame:(CGRect)frame withPresentationStyle:(MMTCSButtonPresentationStyle)style andTitle:(NSString *)title
{
    UIButton* theButton = [[[UIButton alloc] initWithFrame:frame] autorelease];
    
    switch (style) {
        case MMTCSButtonPresentationStyleAccordionHeader:
            [theButton setTitle:title forState:UIControlStateNormal];
            //color and image styles
            [theButton setTitleColor:grayishBlueColor forState:UIControlStateNormal];
            [theButton setBackgroundImage:[UIImage imageNamed:@"tab_bg.png"] forState:UIControlStateNormal];
            //title label styles
            [theButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
            [theButton.titleLabel setShadowColor:[UIColor blackColor]];
            [theButton.titleLabel setShadowOffset:CGSizeMake(0, 1)];
            break;
            
        case MMTCSButtonPresentationStyleRecord:
            [theButton setTitle:title forState:UIControlStateNormal];
            [theButton setBackgroundImage:[UIImage imageNamed:@"record_button.png"] forState:UIControlStateNormal];
            [theButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        default:
            break;
    }
    
    return theButton;
}

-(UILabel *)createLabelWithFrame:(CGRect)frame withPresentationStyle:(MMTCSLabelPresentationStyle)style andText:(NSString *)text
{
    UILabel *theLabel = [[UILabel alloc] initWithFrame:frame];
    [theLabel setText:text];
    
    switch (style) {
        case MMTCSLabelPresentationStyleStatic:
            [theLabel setTextColor:grayishBlueColor]; 
            theLabel.font = [UIFont boldSystemFontOfSize:13];
            theLabel.opaque = NO; 
            theLabel.backgroundColor = [UIColor clearColor];
            break;
            
        case MMTCSLabelPresentationStyleDynamic:
            [theLabel setTextColor:grayColor];
            theLabel.font = [UIFont boldSystemFontOfSize:13];
            theLabel.opaque = NO; 
            theLabel.backgroundColor = [UIColor clearColor];
            break;
            
        case MMTCSLabelPresentationStyleBiometricDataStatic:
            [theLabel setTextColor:grayishBlueColor]; 
            theLabel.font = [UIFont boldSystemFontOfSize:14];
            theLabel.backgroundColor = lightGrayColor;
            theLabel.textAlignment = UITextAlignmentCenter;
            break;
            
        case MMTCSLabelPresentationStyleBiometricDataDynamic:
            theLabel.opaque = NO;
            theLabel.backgroundColor = [UIColor clearColor];
            [theLabel setTextColor:grayColor]; 
            theLabel.font = [UIFont boldSystemFontOfSize:28];
            break;
        default:
            break;
    }
    
    return theLabel;
}

@end
