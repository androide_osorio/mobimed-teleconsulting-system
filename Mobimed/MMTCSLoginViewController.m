//
//  MMTCSLoginViewController.m
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSLoginViewController.h"
#import "MMTCSNetworkManager.h"

/**
 * Private Stuff
 */
@interface MMTCSLoginViewController ()
-(void) applyLabelStyles:(UILabel*)label;
@end

@implementation MMTCSLoginViewController

@synthesize descriptionLabel;
@synthesize usernameField;
@synthesize passwordField1;
@synthesize passwordField2;
@synthesize passwordField3;
@synthesize passwordField4;
@synthesize loginButton;
@synthesize NumpadButtons;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


//--------------------------------------------
/**
 * View Lifecycle
 */
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //agregar fondo, usa una imagen y de ellasaca la info de color y la pone en background
	//es mas rapido q poner una UIImage en fondo en el archivo NIB
	UIColor* backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-splash.png"]];
	[self.view setBackgroundColor:backgroundColor];
	[self applyUserNameTextFieldStyle: usernameField];
    [self applyPasswordTextFieldStyles:passwordField1];
    [self applyPasswordTextFieldStyles:passwordField2];
    [self applyPasswordTextFieldStyles:passwordField3];
    [self applyPasswordTextFieldStyles:passwordField4];
	[self applyLabelStyles:self.descriptionLabel];
}


- (void)viewDidUnload
{
    [self setDescriptionLabel:nil];
    [self setUsernameField:nil];
    [self setPasswordField1:nil];
    [self setPasswordField2:nil];
    [self setPasswordField3:nil];
    [self setPasswordField4:nil];
    [self setLoginButton:nil];
    [self setDelegate:nil];
    [self setNumpadButtons:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)dealloc
{
    [descriptionLabel release];
    [usernameField release];
    [passwordField1 release];
    [passwordField2 release];
    [passwordField3 release];
    [passwordField4 release];
    [loginButton release];
    [delegate release];
    [NumpadButtons release];
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

//-----------------------------------------------------------
/**
 * UITextFieldDelegate Methods
 */
#pragma mark - UITextFieldDelegate Methods

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i=0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i]; 
        if(![myCharSet characterIsMember:c]){
            return NO;
        }
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength >1) ? NO : YES;
}

//este metodo es llamado cuando se hace tap en el boton enter del teclado (o return o done o lo q sea, es siempre el mismo boton)
- (BOOL) textFieldShouldReturn:(UITextField *)theTextField
{
    [theTextField resignFirstResponder];
    return YES;
}

//-----------------------------------------------------------
/**
 * Action Methods
 */
#pragma mark - Action Methods

//make the login handle actions when the login button is pressed
- (IBAction)callLogin:(id)sender
{
    //verify that the username field and password fields are filled with valid data
    if(![self.usernameField.text isEmpty]){
        if(![self.passwordField1.text isEmpty] && ![self.passwordField2.text isEmpty] &&
           ![self.passwordField4.text isEmpty] && ![self.passwordField4.text isEmpty]){
            //create a network manager to handle the login process
            MMTCSNetworkManager *login = [[MMTCSNetworkManager alloc] init];
            //get the final string for the password field
            NSString* pass = [NSString stringWithFormat:@"%@%@%@%@", self.passwordField1.text, self.passwordField2.text,self.passwordField4.text,self.passwordField4.text];
            
            //Call the login method
            [login loginWithUsername:self.usernameField.text andPassword:pass];
            //if the login process is successful, request data from the server to save in core data and tell the delegate
            //it's time to dismiss this view
            if (login.userLoggedIn) {
                [login requestDataFromServer];
                [self.delegate loginViewControllerDidMadeLoginRequest:self];
            }
            else{
                //if it was not successful, promt an UIAlertView.
                UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:@"Error en la Conexión" 
                                                                message:@"Hubo error iniciando sesión. Intentelo de nuevo más tarde." 
                                                               delegate:self 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil] autorelease];
                
                [alert show];
            }
        }
        else{
            //the password is not a valid passcode, prompt the use with the error and tell him to try again
            UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:@"Contraseña inválida" 
                                                             message:@"Introduzca una contraseña válida (codigo de 4 números)." 
                                                            delegate:self 
                                                   cancelButtonTitle:@"OK" 
                                                   otherButtonTitles:nil] autorelease];
            
            [alert show];
        }
    }
    else{
        //if it's not, display a UIAlertView with a message to correct the info and try again
        UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:@"Datos inválidos" 
                                                         message:@"Por favor, introduzca un nombre de usuario o contraseña válidos." 
                                                        delegate:self 
                                               cancelButtonTitle:@"OK" 
                                               otherButtonTitles:nil] autorelease];
        
        [alert show];
    }
    
}

- (IBAction)writePassword:(id)sender 
{
    UIButton *pressed = (UIButton *)sender;
    NSNumber *theTag = [NSNumber numberWithInt:[pressed tag]];
    
    if ([theTag intValue] > 0 && [theTag intValue] <= 10) {
        if([theTag intValue] == 10) theTag = [NSNumber numberWithInt:0];
        if (currentPasswordField == 0) {
            self.passwordField1.text = [theTag stringValue];
        }
        if (currentPasswordField == 1) {
            self.passwordField2.text = [theTag stringValue];
        }
        if (currentPasswordField == 2) {
            self.passwordField3.text = [theTag stringValue];
        }
        if (currentPasswordField == 3) {
            self.passwordField4.text = [theTag stringValue];
        }
        currentPasswordField+=1;
    }
    else{
        if (currentPasswordField == 0) {
            self.passwordField1.text = @"";
        }
        if (currentPasswordField == 1) {
            self.passwordField2.text = @"";
        }
        if (currentPasswordField == 2) {
            self.passwordField3.text = @"";
        }
        if (currentPasswordField == 3) {
            self.passwordField4.text = @"";
        }
        currentPasswordField-=1;
    }
}

- (IBAction)createAccount:(id)sender 
{
    
}

//-----------------------------------------------------------
/**
 * MMTCSCreateAccountViewControllerDelegate Methods
 */
#pragma mark - MMTCSCreateAccountViewControllerDelegate Methods

-(void)createAccountShouldDismiss:(MMTCSCreateAccountViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//-----------------------------------------------------------
/**
 * storyboard Methods
 */
#pragma mark - MMTCSCreateAccountViewControllerDelegate Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"createAccount"])
	{
		MMTCSCreateAccountViewController *createAccountView = segue.destinationViewController;
		createAccountView.delegate = self;
	}
}

//-----------------------------------------------------------
/**
 * Private Methods
 */
#pragma mark - Private Methods

-(void)applyUserNameTextFieldStyle:(UITextField *)textField
{
    [textField setFont:[UIFont boldSystemFontOfSize:16]];	
	[textField setTextColor:[UIColor colorWithRed:0.36 green:0.36 blue:0.46 alpha:1.0]];
	[textField setTextAlignment:UITextAlignmentCenter];
}


-(void)applyLabelStyles:(UILabel *)label
{
    [label setFont:[UIFont systemFontOfSize:18]];	
	[label setTextColor:[UIColor whiteColor]];
	[label setShadowColor:[UIColor blackColor]];
	[label setShadowOffset:CGSizeMake(0, -1)];
	[label setTextAlignment:UITextAlignmentCenter];
	[label setBackgroundColor:[UIColor clearColor]];
}

-(void)applyPasswordTextFieldStyles:(UITextField *)textField
{  
    
   
    [textField setFont:[UIFont boldSystemFontOfSize:24]];
	[textField setTextColor:[UIColor colorWithRed:0.96 green:0.96 blue:0.98 alpha:1.0]];
	[textField setTextAlignment:UITextAlignmentCenter];
    
    textField.secureTextEntry = YES;

}
@end
