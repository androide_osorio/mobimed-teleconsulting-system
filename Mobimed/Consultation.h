//
//  Consultation.h
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface Consultation : NSManagedObject

@property (nonatomic, retain) NSNumber * blood_pressure;
@property (nonatomic, retain) NSNumber * BMI;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSString * current_illness;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * diagnosis;
@property (nonatomic, retain) NSString * general_checklist;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * pronostic;
@property (nonatomic, retain) NSNumber * pulse;
@property (nonatomic, retain) NSString * regional_checklist;
@property (nonatomic, retain) NSNumber * respiratory_rate;
@property (nonatomic, retain) NSNumber * temperature;
@property (nonatomic, retain) NSString * treatment;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) Patient *patient;

@end
