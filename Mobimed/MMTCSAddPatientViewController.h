//
//  MMTCSAddPatientViewController.h
//  Mobimed
//
//  Created by evento on 13/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMTCSAddPatientViewController;
@protocol MMTCSAddPatientViewControllerDelegate <NSObject>

-(void)addPatientDidCancel:(MMTCSAddPatientViewController *)controller;

-(void)addPatientDidSubmit:(MMTCSAddPatientViewController *)controller;

@end

@interface MMTCSAddPatientViewController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property(retain, nonatomic) id<MMTCSAddPatientViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutlet UITextField *nameTextField;

@property (retain, nonatomic) IBOutlet UITextField *ageTextField;

@property (retain, nonatomic) IBOutlet UITextField *genderTextField;


@property (retain, nonatomic) IBOutlet UITextField *phoneTextField;

@property (retain, nonatomic) IBOutlet UITextField *cellphoneTextField;

@property (retain, nonatomic) IBOutlet UITextField *workphoneTextField;

@property (retain, nonatomic) IBOutlet UITextField *emailTextField;

@property (retain, nonatomic) IBOutlet UITextField *maritalStatusTextfield;

@property (retain, nonatomic) IBOutlet UITextField *ethnicityTextField;

@property (retain, nonatomic) IBOutlet UITextField *relogionTextField;

@property (retain, nonatomic) IBOutlet UITextView *clinicalHistoryTextField;

@property (retain, nonatomic) IBOutlet UITextField *EPSTextField;

@property (retain, nonatomic) IBOutlet UITextField *regimenTextField;

@property (retain, nonatomic) IBOutlet UITextField *insuranceNumberTextField;

//------------------------------------------------------------------

#pragma mark - Methods

- (IBAction)cancel:(id)sender;

- (IBAction)submitNewPatient:(id)sender;


@end
