//
//  MMTCSLoginViewController.h
//  Mobimed
//
//  Created by Androide Osorio on 9/05/12.
//  Copyright (c) 2012 Androide Osorio, sebaZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMTCSCreateAccountViewController.h"
/**
 * A Delegate Protocol to handle this view as modal
 */
@class MMTCSLoginViewController;

@protocol MMTCSLoginViewControllerDelegate <NSObject>

-(void)loginViewControllerDidMadeLoginRequest:(MMTCSLoginViewController *)controller;

@end


//--------------------------------------------

@interface MMTCSLoginViewController : UIViewController<UITextFieldDelegate, MMTCSCreateAccountViewControllerDelegate>{
    NSInteger currentPasswordField;
}

/**
 * Properties
 */
#pragma mark - Properties

@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (retain, nonatomic) IBOutlet UITextField *usernameField;

@property (retain, nonatomic) IBOutlet UITextField *passwordField1;

@property (retain, nonatomic) IBOutlet UITextField *passwordField2;

@property (retain, nonatomic) IBOutlet UITextField *passwordField3;

@property (retain, nonatomic) IBOutlet UITextField *passwordField4;

@property (retain, nonatomic) IBOutlet UIButton *loginButton;

@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray *NumpadButtons;

@property (retain, nonatomic) id<MMTCSLoginViewControllerDelegate> delegate;


//--------------------------------------------
#pragma mark - methods

-(IBAction)callLogin:(id)sender;

- (IBAction)writePassword:(id)sender;

-(void)applyPasswordTextFieldStyles:(UITextField *)textField;

-(void)applyUserNameTextFieldStyle:(UITextField *)textField;

@end
