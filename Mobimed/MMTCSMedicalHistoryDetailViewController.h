//
//  MMTCSVideoCallViewController.h
//  Mobimed
//
//  Created by evento on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>


@interface MMTCSMedicalHistoryDetailViewController : UIViewController<UISplitViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UIWebView *historyWebView;

@property (retain, nonatomic) Patient *currentPatient;

- (IBAction)backToDashboard:(id)sender;

@end
