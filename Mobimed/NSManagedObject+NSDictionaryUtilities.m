//
//  NSManagedObject+NSDictionaryUtilities.m
//  Mobimed
//
//  Created by evento on 22/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSManagedObject+NSDictionaryUtilities.h"

@implementation NSManagedObject (NSDictionaryUtilities)

@dynamic traversed;

/**
 * NSManagedObject to NSDictionary (for core data to json)
 */
- (NSDictionary*) toDictionary
{
    self.traversed = YES;
    //get the attributes and relationships by name
    NSArray* attributes       = [[[self entity] attributesByName] allKeys];
    NSArray* relationships    = [[[self entity] relationshipsByName] allKeys];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:
                                 [attributes count] + [relationships count] + 1];
    //set the class key with the class of the ManagedObject
    [dict setObject:[[self class] description] forKey:@"class"];
    
    //loop through the attributes and assign them
    for (NSString* attr in attributes) {
        id value = [self valueForKey:attr];
        
        if (value != nil) {
            //parse the values to the correct type
            NSAttributeType attributeType = [[[[self entity] attributesByName] objectForKey:attr] attributeType];
            if ((attributeType == NSStringAttributeType) && ([value isKindOfClass:[NSNumber class]])) {
                value = (NSString *)[value stringValue];
            }
            else if (((attributeType == NSInteger16AttributeType) || (attributeType == NSInteger32AttributeType) || (attributeType == NSInteger64AttributeType) || (attributeType == NSBooleanAttributeType)) && ([value isKindOfClass:[NSString class]])) {
                value = (NSNumber *)[NSNumber numberWithInteger:[value integerValue]];
            }
            else if ((attributeType == NSFloatAttributeType) &&  ([value isKindOfClass:[NSString class]])) {
                value = (NSNumber *)[NSNumber numberWithDouble:[value doubleValue]];
            }
            else if ((attributeType == NSDateAttributeType) && ([value isKindOfClass:[NSString class]])) {
                NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                value = (NSDate *)[formatter dateFromString:value];
            }
            [dict setObject:value forKey:attr];
        }
    }
    //loop through all the relationships and add them correctly to the dictionary
    for (NSString* relationship in relationships) {
        NSObject* value = [self valueForKey:relationship];
        
        if ([value isKindOfClass:[NSSet class]]) {
            // To-many relationship
            
            // The core data set holds a collection of managed objects
            NSSet* relatedObjects = (NSSet*) value;
            
            // Our set holds a collection of dictionaries
            NSMutableSet* dictSet = [NSMutableSet setWithCapacity:[relatedObjects count]];
            
            for (NSManagedObject* relatedObject in relatedObjects) {
                if (!relatedObject.traversed) {
                    [dictSet addObject:[relatedObject toDictionary]];
                }
            }
            
            [dict setObject:dictSet forKey:relationship];
        }
        else if ([value isKindOfClass:[NSManagedObject class]]) {
            // To-one relationship
            
            NSManagedObject* relatedObject = (NSManagedObject*) value;
            
            if (!relatedObject.traversed) {
                // Call toDictionary on the referenced object and put the result back into our dictionary.
                [dict setObject:[relatedObject toDictionary] forKey:relationship];
            }
        }
    }
    
    return dict;
}


- (void) populateFromDictionary:(NSDictionary*)dict
{
    NSManagedObjectContext* context = [self managedObjectContext];
    //loop through the dictionary
    for (NSString* key in dict) {
        //ignore the class key attribute
        if ([key isEqualToString:@"class"]) {
            continue;
        }
        //get the value for the current key
        id value = [dict objectForKey:key];
        
        if ([value isKindOfClass:[NSDictionary class]]) {
            // This is a to-one relationship
            NSManagedObject* relatedObject =
            [NSManagedObject createManagedObjectFromDictionary:(NSDictionary*)value
                                                           inContext:context];
            
            [self setValue:relatedObject forKey:key];
        }
        else if ([value isKindOfClass:[NSSet class]] || [value isKindOfClass:[NSArray class]]) {
            // This is a to-many relationship
            NSSet* relatedObjectDictionaries;
            if([value isKindOfClass:[NSArray class]]){
                relatedObjectDictionaries = [NSSet setWithArray:value];
            }
            else{
                relatedObjectDictionaries = (NSSet *)value;
            }
            
            // Get a proxy set that represents the relationship, and add related objects to it.
            // (Note: this is provided by Core Data)
            NSMutableSet* relatedObjects = [self mutableSetValueForKey:key];
            
            for (NSDictionary* relatedObjectDict in relatedObjectDictionaries) {
                NSManagedObject* relatedObject =
                [NSManagedObject createManagedObjectFromDictionary:relatedObjectDict
                                                               inContext:context];
                [relatedObjects addObject:relatedObject];
            }
        }
        else if (value != nil) {
            /**
             * this for correctly parsing the attribute types between the json and core data objects
             */
            NSAttributeType attributeType = [[[[self entity] attributesByName] objectForKey:key] attributeType];
            //convert the numbers to strings if the field is a string
            if ((attributeType == NSStringAttributeType) && ([value isKindOfClass:[NSNumber class]])) {
                value = (NSString *)[value stringValue];
            } 
            else if (((attributeType == NSInteger16AttributeType) || (attributeType == NSInteger32AttributeType) || (attributeType == NSInteger64AttributeType) || (attributeType == NSBooleanAttributeType)) && ([value isKindOfClass:[NSString class]])) {
                //convert the numeric integer values to NSNumbers
                value = (NSNumber *)[NSNumber numberWithInteger:[value integerValue]];
            }
            else if ((attributeType == NSFloatAttributeType) &&  ([value isKindOfClass:[NSString class]])) {
                //convert the floating point and decimal numbers (usually they come as string) to actual numbers
                value = (NSNumber *)[NSNumber numberWithDouble:[value doubleValue]];
            }
            else if ((attributeType == NSDateAttributeType) && ([value isKindOfClass:[NSString class]])) {
                //if it is a date, parse the string with the given date format.
                NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                value = (NSDate *)[formatter dateFromString:value];
            }

            // This is an attribute
            [self setValue:value forKey:key];
        }
    }
}


+ (NSManagedObject*) createManagedObjectFromDictionary:(NSDictionary*)dict
                                                   inContext:(NSManagedObjectContext*)context
{
    NSString* class = [dict objectForKey:@"class"];
    NSManagedObject* newObject =
    (NSManagedObject*)[NSEntityDescription insertNewObjectForEntityForName:class
                                                          inManagedObjectContext:context];
    
    [newObject populateFromDictionary:dict];
    
    return newObject;
}

@end
