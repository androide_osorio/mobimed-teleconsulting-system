//
//  MMTCSCreateAccountViewController.m
//  Mobimed
//
//  Created by evento on 13/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSCreateAccountViewController.h"

@implementation MMTCSCreateAccountViewController
@synthesize formContainer;
@synthesize createAccountTextFields;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor* backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-splash.png"]];
	[self.view setBackgroundColor:backgroundColor];
    
    self.formContainer.layer.cornerRadius = 20.0f;
    self.formContainer.layer.borderColor  = [UIColor colorWithWhite:158/255 alpha:1.0].CGColor;
    self.formContainer.layer.borderWidth  = 1.0;
    self.formContainer.layer.shadowOffset = CGSizeMake(0, -1);
    self.formContainer.layer.shadowRadius = 20.0;
    self.formContainer.layer.shadowOpacity = 0.2;
    self.formContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    
    self.formContainer.layer.masksToBounds = YES;
}


- (void)viewDidUnload
{
    [self setFormContainer:nil];
    [self setCreateAccountTextFields:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)dealloc {
    [formContainer release];
    [delegate release];
    [createAccountTextFields release];
    [super dealloc];
}


#pragma mark - IBAction methods

- (IBAction)returnToLoginScreen:(id)sender 
{
    [delegate createAccountShouldDismiss:self];
}

- (IBAction)createAccount:(id)sender 
{
    [delegate createAccountShouldDismiss:self];
}

@end
