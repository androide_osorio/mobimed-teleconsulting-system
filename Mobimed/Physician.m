//
//  Physician.m
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Physician.h"
#import "Patient.h"


@implementation Physician

@dynamic address;
@dynamic age;
@dynamic cellphone;
@dynamic id;
@dynamic last_entry;
@dynamic mayor;
@dynamic name;
@dynamic passcode;
@dynamic phone;
@dynamic photo;
@dynamic speciality;
@dynamic patient;

@end
