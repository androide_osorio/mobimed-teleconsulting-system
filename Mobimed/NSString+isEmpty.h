//
//  NSString+isEmpty.h
//  Mobimed
//
//  Created by evento on 22/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (isEmpty)

-(BOOL)isEmpty;

-(BOOL)isNumeric;

@end
