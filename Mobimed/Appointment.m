//
//  Appointment.m
//  Mobimed
//
//  Created by evento on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Appointment.h"
#import "Patient.h"


@implementation Appointment

@dynamic date;
@dynamic duration;
@dynamic id;
@dynamic place;
@dynamic title;
@dynamic patient;

@end
