//
//  MMTCSPatientsTileView.m
//  Mobimed
//
//  Created by evento on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MMTCSPatientsTileView.h"

/////////////////////
//
/////////////////////
#pragma mark - private Methods and stuff
@interface MMTCSPatientsTileView ()

-(void)addStaticLabelStyles:(UILabel*)label withView:(UIView *)view;

-(void)addDinamicLabelStyles:(UILabel *)label  withView:(UIView *)view;

-(void)addHeaderNameLabelStyles:(UILabel *)label  withView:(UIView *)view;

@end


//-----------------------------------------------------------------------------------------
/////
//
/////
@implementation MMTCSPatientsTileView

#pragma mark - Constants
const CGFloat infoContainerHeight = 130.0;
static UIColor* grayColor;
static UIColor* grayishBlueColor;

#pragma mark - Synthesize properties

@synthesize presentationStyle, pictureUIImageView, patientNameLabel, hourLabel;

@synthesize placeLabel, durationLabel, dateLabel;

#pragma mark - initialization Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithPresentationStyle:(PatientsTileViewPresentationStyle)style
{
    CGRect frame;
    frame.origin = CGPointMake(0, 0);
    
    //set the frame according to the presentation style
    switch (style) {
        case PatientsTileViewPresentationStylePhoto:
            //special tile for today's consults
            stylePhotoObjWidth = 256.0;
            stylePhotoObjHeight = 386.0;
            frame.size = CGSizeMake(stylePhotoObjWidth, stylePhotoObjHeight);
            break;
            
        case PatientsTileViewPresentationStyleSimple:
            //citas proximas
            styleSimpleObjWidth = 189.0;
            styleSimpleObjHeight = 108.0;
            frame.size = CGSizeMake(styleSimpleObjWidth, 190);
            
            break;
    }
    self = [super initWithFrame:frame];
    
    if (self) {
        self.presentationStyle = style;
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
		grayColor        = [UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:1];
		grayishBlueColor = [UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1];
		
		patientNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 255, 250, 50)];
		hourLabel        = [[UILabel alloc] initWithFrame:CGRectMake(-30, 290, 250, 50)];
		placeLabel       = [[UILabel alloc] initWithFrame:CGRectMake(-30, 308, 250, 50)];
		durationLabel    = [[UILabel alloc] initWithFrame:CGRectMake(-30,326, 250, 50)];
		dateLabel        = [[UILabel alloc] initWithFrame:CGRectMake(100, 140, 250, 50)];
		
		staticHourLabel     = [[UILabel alloc] initWithFrame:CGRectMake(30, 290, 100, 50)];
		staticPlaceLabel    = [[UILabel alloc] initWithFrame:CGRectMake(30, 308, 100, 50)];
		staticDurationLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 326, 100, 50)];
		staticDateLabel     = [[UILabel alloc] initWithFrame:CGRectMake(50, 140, 100, 50)];
        
        [staticHourLabel setText:@"Hora Consulta"];
        
        [staticPlaceLabel setText:@"Lugar"];
        
        [staticDurationLabel setText:@"Duración"];
        
        [staticDateLabel setText:@"fecha"];
	}
    
    return self;
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
   
    switch(self.presentationStyle){
        case PatientsTileViewPresentationStylePhoto:
            
			pictureUIImageView.frame = rect;
			self.clipsToBounds       = YES;
            
			//Genera contenedor Labels
            UIView* contenedorInfo = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(rect), CGRectGetMaxY(rect)-infoContainerHeight, stylePhotoObjWidth, infoContainerHeight)];
            contenedorInfo.userInteractionEnabled =NO;
            contenedorInfo.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
            [self addSubview:contenedorInfo];
            //[contenedorInfo release];
            
            //the labels
            [self addHeaderNameLabelStyles:self.patientNameLabel withView:self]; //encabezadoNombrePaciente
            [self addStaticLabelStyles:staticHourLabel withView:self];
            
            [self addStaticLabelStyles:staticPlaceLabel withView:self];
            
            [self addStaticLabelStyles:staticDurationLabel withView:self];
            
            //the dynamic labels
            [self addDinamicLabelStyles:self.hourLabel withView:self];
            [self addDinamicLabelStyles:self.placeLabel withView:self];
            [self addDinamicLabelStyles:self.durationLabel withView:self];
            break;
            
        case PatientsTileViewPresentationStyleSimple:
            
            pictureUIImageView.frame = CGRectMake(30, 15, styleSimpleObjWidth, styleSimpleObjHeight);
            
            pictureUIImageView.backgroundColor = [UIColor whiteColor];
            [self addHeaderNameLabelStyles:self.patientNameLabel withView:self]; //encabezadoNombrePaciente
            [self addDinamicLabelStyles:self.dateLabel withView:self];
            [self addStaticLabelStyles:staticDateLabel withView:self];
            [staticDateLabel setText:@"Fecha"];
            pictureUIImageView.layer.cornerRadius = 13.0;
            pictureUIImageView.layer.masksToBounds = YES;
            break;
    }
}

//------------------------------------------------

#pragma mark - Memory Management
-(void)dealloc
{
    [pictureUIImageView release];
    [patientNameLabel release];
    [placeLabel release];
    [hourLabel release];
    [dateLabel release];
    [durationLabel release];
    [staticDateLabel release];
    [staticDurationLabel release];
    [staticHourLabel release];
    [staticPlaceLabel release];

    
    [super dealloc];
}

//------------------------------------------------
#pragma mark - Private and Helper Methods

// recibe una imagen que la ubica en el mismo en el UIImageView y luego en el UIView. Setea informacion en los labels

-(void)setPatientImage:(UIImage*)image withInfo:(NSString*)nombre_ 
              dateHour:(NSString*)horaConsulta_ place:(NSString*)lugar_ 
              duration:(NSString*)duracion_ andDate:(NSString*)fechaConsulta_;
{
	if (!pictureUIImageView) {
        //if(image.size.width>239 && image.size.height>158){
            //CGSize size = CGSizeMake(,size1.height);
        //}
        pictureUIImageView = [[UIImageView alloc] initWithImage:image];
        pictureUIImageView.frame = CGRectMake(40, 40, styleSimpleObjWidth, styleSimpleObjHeight);
        
		[self addSubview:pictureUIImageView];  // 
        [self sendSubviewToBack:pictureUIImageView];
    }
 	
	[self.patientNameLabel setText:nombre_]; 
	[self.hourLabel setText:horaConsulta_];
	[self.placeLabel setText:lugar_];
	[self.durationLabel setText:duracion_];
	[self.dateLabel setText:fechaConsulta_];
}

-(void)addStaticLabelStyles:(UILabel*)label withView:(UIView *)view
{
	switch (self.presentationStyle) {
		case PatientsTileViewPresentationStylePhoto:
			label.font = [UIFont boldSystemFontOfSize:13];
            label.textAlignment = UITextAlignmentLeft;
			break;
		case PatientsTileViewPresentationStyleSimple:
			label.font = [UIFont boldSystemFontOfSize:12];
			break;
	}
	[label setTextColor:[UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1]];
    label.opaque = NO; 
    label.backgroundColor = [UIColor clearColor];
	
    [view addSubview:label];
}

-(void)addDinamicLabelStyles:(UILabel *)label  withView:(UIView *)view
{
	
	switch (self.presentationStyle) {
		case PatientsTileViewPresentationStylePhoto:
			label.font = [UIFont boldSystemFontOfSize:13];
            label.textAlignment = UITextAlignmentRight;
			break;
		case PatientsTileViewPresentationStyleSimple:
			label.font = [UIFont boldSystemFontOfSize:12];
			break;
	}
	
	[label setTextColor:[UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:1]];
	label.opaque = NO; 
	label.backgroundColor = [UIColor clearColor];
    [view addSubview:label];
}

-(void)addHeaderNameLabelStyles:(UILabel *)label  withView:(UIView *)view
{
	CGRect frame = label.frame;
	switch (self.presentationStyle) {
		case PatientsTileViewPresentationStylePhoto:
			label.font = [UIFont boldSystemFontOfSize:18];
			[label setTextColor:[UIColor colorWithRed:103.0/255 green:142.0/255 blue:141.0/255 alpha:1]];
			break;
            
		case PatientsTileViewPresentationStyleSimple:
            frame.origin = CGPointMake(0, 120);
            [label setFrame:frame];
			label.font = [UIFont boldSystemFontOfSize:15];
			[label setTextColor:[UIColor colorWithRed:104.0/255 green:104.0/255 blue:104.0/255 alpha:1]];
			break;
	}
    label.textAlignment = UITextAlignmentCenter;
	label.opaque = NO; 
	label.backgroundColor = [UIColor clearColor];
    [view addSubview:label];
}

//------------------------------------------------------
#pragma mark - Getters and Setters


-(CGFloat)stylePhotoObjWidth
{
    return stylePhotoObjWidth;
}

-(void)setStylePhotoObjWidth:(CGFloat)width
{
    stylePhotoObjWidth = width;
}

-(CGFloat)stylePhotoObjHeight
{
    return stylePhotoObjHeight;
}

-(void)setStylePhotoObjHeight:(CGFloat)height
{
    stylePhotoObjHeight= height;
}


-(CGFloat)styleSimpleObjWidth{
    return styleSimpleObjWidth;
}

-(void)setStyleSimpleObjWidth:(CGFloat)width
{
    styleSimpleObjWidth = width;
}

-(CGFloat)styleSimpleObjHeight{
    
    return styleSimpleObjHeight;
}

-(void)setStyleSimpleObjHeight:(CGFloat)height
{
    styleSimpleObjHeight = height;
}


@end
