//
//  MMTCSPatientsListViewController.h
//  Mobimed
//
//  Created by evento on 24/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  MMTCSPatientsListViewController;
@protocol MMTCSPatientsListViewControllerDelegate <NSObject>

-(void)patientsListViewControllerDidCancel:(MMTCSPatientsListViewController *)controller;

-(void)patientsListViewControllerDidSubmit:(MMTCSPatientsListViewController *)controller;

@end

//-------------------------------------------------------------------

@interface MMTCSPatientsListViewController : UITableViewController

@property (retain, nonatomic) NSArray *patients;

@property (retain, nonatomic) Patient *selectedPatient;

@property (retain, nonatomic) id<MMTCSPatientsListViewControllerDelegate> delegate;

- (IBAction)cancel:(id)sender;
@end
