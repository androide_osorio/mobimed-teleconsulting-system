//
//  MMTCSAppointmentDateViewController.m
//  Mobimed
//
//  Created by Androide Osorio on 17/05/12.
//  Copyright (c) 2012 Androide Osorio, sebaZ. All rights reserved.
//

#import "MMTCSAppointmentDateViewController.h"

@implementation MMTCSAppointmentDateViewController

#pragma mark - Synthesized Properties
@synthesize delegate;
@synthesize initDateCell;
@synthesize endDateCell;
@synthesize datePicker;
@synthesize currentDate;

//
//

#pragma mark - Initialization and memory management methods

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {
    [datePicker release];
    [initDateCell release];
    [endDateCell release];
    [delegate release];
    [super dealloc];
}

//-----------------------------------------------------------------------------------------------------

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-splash.png"]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [self setInitDateCell:nil];
    [self setEndDateCell:nil];

    [self setDelegate:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tableView.backgroundColor = [UIColor clearColor];
    //self.tableView.backgroundView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-splash.png"]];
    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255 blue:85.0/255 alpha:1];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    NSDate* theDate   = [datePicker date];
    NSDate* laterDate = [theDate dateByAddingTimeInterval:60*60];
    NSDateFormatter* format     = [[NSDateFormatter alloc] init];
    NSDateFormatter* hourFormat = [[NSDateFormatter alloc] init];
    
    [format setDateFormat:@"EEE dd MMMM yyyy HH:mm"];
    [hourFormat setDateFormat:@"HH:mm"];
    
    // Configure the cell...
    if(indexPath.row == 0){
        if(![[datePicker date] isEqualToDate:theDate])
        [datePicker setDate:theDate animated:YES];
        initDateCell.detailTextLabel.text = [format stringFromDate:theDate];
        
        cell = initDateCell;
    }
    else if(indexPath.row == 1){
        if(![[datePicker date] isEqualToDate:laterDate])
        [datePicker setDate:laterDate animated:YES];
        
        endDateCell.detailTextLabel.text = [hourFormat stringFromDate:laterDate];
        
        cell = endDateCell;
    }
    
    [hourFormat release];
    [format release];
    return cell;
}


//------------------------------------------------------------------------------------------
/**
 * Actions connected to the storyboard
 */
#pragma mark - Action Methods

//handles the valueChanged event of th UIDatePicker
- (IBAction)dateChanged:(id)sender
{
    self.currentDate  = [datePicker date];
    NSDate* laterDate = [currentDate dateByAddingTimeInterval:60*60];
    NSDateFormatter* format     = [[NSDateFormatter alloc] init];
    NSDateFormatter* hourFormat = [[NSDateFormatter alloc] init];
    
    [format setDateFormat:@"EEE dd MMMM yyyy HH:mm"];
    [hourFormat setDateFormat:@"HH:mm"];
    

    if(![[datePicker date] isEqualToDate:self.currentDate])
        [datePicker setDate:self.currentDate animated:YES];
    initDateCell.detailTextLabel.text = [format stringFromDate:self.currentDate];
        

    /*if(![[datePicker date] isEqualToDate:laterDate])
        [datePicker setDate:laterDate animated:YES];*/
        
    endDateCell.detailTextLabel.text = [hourFormat stringFromDate:laterDate];
    
    [hourFormat release];
    [format release];
}

//submit the date and dismiss this controller
- (IBAction)submitDate:(id)sender
{
    [self.delegate viewcontrollerDidSubmitDate:self];
}


@end
