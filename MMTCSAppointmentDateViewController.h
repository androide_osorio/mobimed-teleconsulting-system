//
//  MMTCSAppointmentDateViewController.h
//  Mobimed
//
//  Created by evento on 17/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMTCSAppointmentDateViewController;
@protocol MMTCSAppointmentDateViewControllerDelegate <NSObject>

- (void)viewcontrollerDidSubmitDate:(MMTCSAppointmentDateViewController *)controller;

@end
//-----------------------------------------------------------------------

@interface MMTCSAppointmentDateViewController : UITableViewController

#pragma mark - Properties

@property (retain, nonatomic) IBOutlet UITableViewCell *initDateCell;

@property (retain, nonatomic) IBOutlet UITableViewCell *endDateCell;

@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (assign, nonatomic) NSDate *currentDate;

@property (retain, nonatomic) id<MMTCSAppointmentDateViewControllerDelegate> delegate;


#pragma mark - Controller Actions
- (IBAction)dateChanged:(id)sender;

- (IBAction)submitDate:(id)sender;

@end
